<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasicEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basic_entries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('client_id');
            $table->bigInteger('category_id');
            $table->bigInteger('district_id');
            $table->string('name');
            $table->string('slug');
            $table->string('logo');
            $table->string('website')->nullable();
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('image');
            $table->text('profile');
            $table->string('metatitle');
            $table->text('metadescription');
            $table->text('metakeywords')->nullable();
            $table->boolean('newlife');
            $table->boolean('active');
            $table->bigInteger('order');
            $table->date('start_at');
            $table->date('end_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('basic_entries');
    }
}
