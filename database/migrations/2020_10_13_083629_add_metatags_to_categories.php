<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMetatagsToCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->string('metatitle')->after('image')->nullable();
            $table->string('metadescription')->after('metatitle')->nullable();
            $table->string('metakeywords')->after('metadescription')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->string('metatitle')->after('image')->nullable();
            $table->string('metadescription')->after('metatitle')->nullable();
            $table->string('metakeywords')->after('metadescription')->nullable();
        });
    }
}
