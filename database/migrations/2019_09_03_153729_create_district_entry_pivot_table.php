<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistrictEntryPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('district_entry', function (Blueprint $table) {
            $table->bigInteger('district_id')->unsigned()->index();
            // $table->foreign('district_id')->references('id')->on('districts')->onDelete('cascade');
            $table->bigInteger('entry_id')->unsigned()->index();
            // $table->foreign('entry_id')->references('id')->on('entries')->onDelete('cascade');
            $table->primary(['district_id', 'entry_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('district_entry');
    }
}
