$(document).ready(function(){
    var _token = $('input[name="_token"]').val();


    //Admin scripts
    $('#delete').find('button').on('click', function(){
        return confirm("Θέλετε σίγουρα να προχωρήσετε σε διαγραφή;");
    });


    // ajax call για διαγραφή shop
    $('.deleteShop').on('click', function(){

        let shop = $(this).data("shop");

        // alert(shop);

        $.ajax({
            method: "DELETE",
            url: "/admin/shop/"+shop+"/delete",
            data: {"shop": shop,  "_token":_token},
            success:  $(this).hide().closest('.card').find('input').val('')
        });
    });

    // Sortable Categories

    $('#categorySortable').sortable({
        axis: 'y',
        update: function(event, ui) {

            var order = $(this).sortable('toArray');

            $.post("/admin/categories/order", {order:order, _token:_token});
        }
    });

    // Sortable SubCategories

    $('#subcategorySortable').sortable({
        axis: 'y',
        update: function(event, ui){
            var order = $(this).sortable('toArray');

            $.post("/admin/categories/suborder", {order:order, _token:_token});
        }
    });

    // Sortable albums
    $('#albumSortable').sortable({
        axis: 'y',
        update: function(event, ui){
            var order = $(this).sortable('toArray');       
            var entry = order[0].split('-')[0];        

            $.post("/admin/entries/" + entry + "/albums/order", {order:order, _token:_token});
        }
    });

    // Sortable Images
    $('#imageSortable').sortable({
        update: function(event, ui){
            var order = $(this).sortable('toArray');
            var entry = order[0].split('-')[0];
            var album = order[0].split('-')[1];

            $.post("/admin/entries/"+entry+"/albums/"+album+"/images/order", {order:order, _token:_token});
        }
    });

    // Sortable Videos
    $('#videoSortable').sortable({
        update: function(event, ui){
            var order = $(this).sortable('toArray');
            var entry = order[0].split('-')[0];
            
            $.post("/admin/entries/"+entry+"/video/order", {order:order, _token:_token});
        }
    });

    // Sortable Pages
    $('#pagesSortable').sortable({
        update: function(event, ui){
            var order = $(this).sortable('toArray');

            $.post("/admin/pages/order", {order:order, _token:_token});
        }
    });

    // Sortable infoPages
    $('#infoPagesSortable').sortable({
        update: function(event, ui){
            var order = $(this).sortable('toArray');

            $.post("/admin/infopages/order", {order:order, _token:_token});
        }
    });

    // Check All
    $("#checkAll").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });

});