<h3 class="text-center py-2">ΒΑΠΤΙΣΗ</h3>
<ul class="p-0">            
    @foreach ($categories->where('parent_id', 1) as $category)
        <li class=""><a href="/{{$category->slug}}">{{$category->name}}</a></li>
    @endforeach
</ul>
@include('site.footer.copyright')