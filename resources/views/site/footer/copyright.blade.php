<div class="copyright text-center mt-5">
    {{\Carbon\Carbon::now()->format('Y')}} <i class="fa fa-copyright" aria-hidden="true"></i> VaptisiPortal.gr
</div>