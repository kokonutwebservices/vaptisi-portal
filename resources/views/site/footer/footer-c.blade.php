<h3 class="text-center py-2">ΠΛΗΡΟΦΟΡΙΕΣ</h3>
<ul class="p-0">
    @foreach ($infoPages as $page)
        <li class=""><a href="/{{$page->slug}}">{{$page->title}}</a></li>        
    @endforeach
</ul>
<h3 class="text-center py-2 mt-2">ΓΑΜΟΣ</h3>
<ul class="p-0">
    <li class="">
        <a href="https://www.gamosportal.gr" target="_blank" title="Σε κάθε βήμα του γάμου, Gamos Portal"><img src="/storage/gamosportal-logo.png" alt="Σε κάθε βήμα του γάμου, Gamos Portal" title="Σε κάθε βήμα του γάμου, Gamos Portal"></a>
    </li>
</ul> 

