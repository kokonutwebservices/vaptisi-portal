<h3 class="text-center py-2">ΔΕΞΙΩΣΗ</h3>
<ul class="p-0">
    @foreach ($categories->where('parent_id', 2) as $category)
        <li class=""><a href="/{{$category->slug}}">{{$category->name}}</a></li>
    @endforeach
</ul>
