@extends('site.layouts.master')

@section('metatags')
    <title> {{$page->category->metatitle ?? config('app.name', '')}} </title>
    <meta name="description" content="{{$page->category->metadescription}}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://www.vaptisportal.gr/{{$page->category->slug}}">
    <meta property="og:title" content="{{$page->category->metatitle}}">
    <meta property="og:description" content="{{$page->category->metadescription}}">
    <meta property="og:image" content="https://www.vaptisiportal.gr/storage/categories/{{$page->category->image}}">
    <link rel="canonical" href="{{ config('app.url') }}/{{ $page->slug }}" />
@endsection

@section('content')

<div class="container-fluid {{$page->category->parent_id == 1 ? 'first' : 'second'}}">
   
{{-- Breadcrumbs --}}  
<nav aria-label="breadcrumb">
    <ol class="breadcrumb py-2">
        <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i></a></li>
        <li class="breadcrumb-item"><a href="/{{$page->slug}}">{{$page->category->name}}</a></li>
    </ol>
</nav>
{{-- End Breadcrumbs --}}
    <h1 class="col-md-12 text-center mb-3"> {{$page->title}} </h1>
    <p> {!! $page->category->description !!} </p>

    <div class="districts-filter my-5">        

        @foreach ($districts as $district)
        
            <a class="btn btn-dark mb-2" href="/{{$page->slug}}/p-{{$district->slug}}">
                {{$district->name}}
            </a>
        
        @endforeach

    </div>

    <div class="entries d-md-flex flex-wrap justify-content-center">

        @foreach ($entries as $entry)

            <div class="entry-item col-md-3 mb-3">
                <a href="/{{$page->slug}}/{{$entry->slug}}">
                <img loading="lazy" src="/storage/entries/small/{{$entry->image}}" alt="{{$entry->name}}" title="{{$entry->name}}">
                <h5 class="text-center pt-2 pb-0 mb-0">
                    {{ $entry->name }}
                </h5>
                </a>
                <div class="districts px-5 pb-1 text-center">
                    @foreach ($entry->district as $district)
                        <small><a class="text-light" href="{{$entry->category->slug}}/p-{{$district->slug}}">{{$district->name}}</a></small>
                    @endforeach
                </div>                
            </div>
            
        @endforeach

    </div>

    <div class="m-3 float-right">
        <a class="btn btn-dark" href="/"><i class="fas fa-undo-alt"></i> <i class="fas fa-home"></i> </a>
    </div>
</div>
@endsection