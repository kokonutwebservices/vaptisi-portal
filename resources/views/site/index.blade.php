@extends('site.layouts.master')

@section('metatags')
    <title>Βάπτιση - Vaptisi Portal - Βρείτε τα πάντα για τη βαπτιση</title>
    <meta name="description" content="Βρείτε κορυφαίους επαγγελματίες και ιδέες για βάπτιση, με τον πιο εύκολο τρόπο, στο Vaptisi Portal.">
    <meta name="keywords" content="βάπτιση, vaptisi, βάφτιση, βαφτίσια, βαφτιστικά, vaftisi">
    <meta property="og:title" content="Βάπτιση - Vaptisi Portal - Βρείτε τα πάντα για τη βαπτιση">
    <meta property="og:url" content="https://www.vaptisiportal.gr">
    <meta property="og:type" content="website">
    <meta property="og:description" content="Βρείτε κορυφαίους επαγγελματίες και ιδέες για βάπτιση, με τον πιο εύκολο τρόπο, στο Vaptisi Portal.">
    <meta property="og:site_name" content="VaptisiPortal.gr">
    <meta property="og:image" content="http://vaptisiportal.gr/storage/main.jpg">
@endsection

@section('content')

    <section id="showcase">
        <div class="container">
            @include('site.homepage.showcase')
        </div>
    </section>

    <section id="featured">
        <div class="container">
            @include('site.homepage.welcome')
        </div>
    </section>

    <section id="main">
        <div class="container">
            @include('site.homepage.categories')
        </div>
    </section>

    <section id="latest">
        <div class="container">
            @include('site.homepage.latest')
        </div>
    </section>
 
    
@endsection

