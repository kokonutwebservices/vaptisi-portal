<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @yield('metatags')
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="p:domain_verify" content="478c3ee858e427426700ea09898d133d"/> <!-- Pinterest verification-->
    @include('site.layouts.analytics')
    @include('site.layouts.webmasters')
    @include('site.layouts.quantcast-consent')
    @include('site.layouts.favicon')

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    @yield('stylesheets')
    <livewire:styles />
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    @include('site.layouts.facebook-pixel')
</head>
<body class="d-flex flex-column h-100">
<header>
    <div class="header-strip p-0"></div>
    <div class="container">
        @include('site.layouts.social')
    </div>
    <div class="container-fluid">
        @include('site.layouts.header')
    </div>
    <div class="container-fluid px-0">
        @include('site.layouts.nav')
    </div>
</header>


@yield('content') 
    


<footer id="footer">
    <div class="container-fluid">
        <div class="row">
            @include('site.layouts.footer')
        </div>
    </div>
</footer>


@yield('scripts')
<livewire:scripts />

<script src="https://kit.fontawesome.com/f8ee92d2c6.js" crossorigin="anonymous"></script>
<script src="{{ mix('/js/app.js') }}" defer></script>
@include('site.layouts.quantcast-measure')
<script src="//instant.page/5.1.0" type="module" integrity="sha384-by67kQnR+pyfy8yWP4kPO12fHKRLHZPfEsiSXR8u2IKcTdxD805MGUXBzVPnkLHw"></script>
<script defer src="https://cdn.jsdelivr.net/npm/alpinejs@3.x.x/dist/cdn.min.js"></script>
@yield('photoswipe')
</body>
</html>