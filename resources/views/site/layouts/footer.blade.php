<div class="col-md-4 footer-a py-4">
    @include('site.footer.footer-a')
</div>
<div class="col-md-4 footer-b py-4">
    @include('site.footer.footer-b')
</div>
<div class="col-md-4 footer-c py-4">
    @include('site.footer.footer-c')
    @include('site.footer.signature')
</div>

