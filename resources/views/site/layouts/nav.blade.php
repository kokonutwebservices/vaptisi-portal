<nav class="navbar navbar-expand-lg bg-transparent border-top border-bottom my-1">
  <div class="container">
  
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-center" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          
        </li>
        <li class="nav-item dropdown mr-4">
          <a class="nav-link dropdown-toggle text-dark" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            ΒΑΠΤΙΣΗ
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">

              @foreach ($vaptisiCategories as $category)
                <a href="/{{ $category->slug }}" class="dropdown-item"> {{$category->name}} </a>                
              @endforeach
          </div>
        </li>
        <li class="nav-item dropdown mr-4">
          <a class="nav-link dropdown-toggle text-dark" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            ΔΕΞΙΩΣΗ
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">

              @foreach ($dexiosiCategories as $category)
                <a href="/{{ $category->page->slug }}" class="dropdown-item"> {{$category->name}} </a>                
              @endforeach
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>
