<div class="row">
    <div class="col-md-2">
        <div class="social d-flex flex-wrap justify-content-around ">
            <div class="social-item facebook">
                <a class="text-body" href="https://www.facebook.com/vaptisiportal" target="_blank" rel="nofollow"><i class="fab fa-facebook-f"></i></a>
            </div>
            <div class="social-item instagram">
                <a class="text-body" href="https://www.instagram.com/vaptisiportal/" target="_blank" rel="nofollow"><i class="fab fa-instagram"></i></a>
            </div>
            <div class="social-item pinterest">
                <a class="text-body" href="https://gr.pinterest.com/vaptisiportal" target="_blank" rel="nofollow"><i class="fab fa-pinterest-p"></i></a>
            </div>
        </div>
        
    </div>
</div>
