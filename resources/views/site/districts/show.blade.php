@extends('site.layouts.master')

@section('content')

@foreach ($categories as $category)
   
    @if ($category->parent_id == 0)

    <div class="row px-0 my-5">
        <h2 class="col-12 px-0">{{ $category->name }}</h2>

        <div class="row justify-content-center">
            @foreach (\App\Category::where('parent_id', $category->id)->orderBy('order')->get() as $subcategory)

                @if ($subcategory->categoryEntries($district))
                    <div class="col-md-3 my-5"> 
                        <a href="/{{$district->slug}}/{{$subcategory->slug}}">
                        <img class="rounded-circle" src="{{ $subcategory->image ? "/storage/categories/$subcategory->image" : "/storage/default.png" }}" alt="">
                            <h5 class="text-center">{{ $subcategory->name }} <span>({{$subcategory->categoryEntries($district)}})</span></h5>
                        </a>
                        
                    </div>
                    
                @endif            
                
            @endforeach
        </div>
    </div>
    
        
    @endif
        

@endforeach
    
@endsection