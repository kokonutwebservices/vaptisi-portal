<div class="latest">
    <h2 class="ml-md-2 p-0 p-md-2 my-3 col-md-12 ">ΔΕΙΤΕ ΣΤΟ VAPTISIPORTAL.GR</h2>
    <div class="d-flex flex-wrap mb-5">
        @foreach ($latest as $entry)
            <div class="entry-item col-md-3 mb-3 {{ $entry->category->parent_id == 1 ? 'first' : 'second' }}">
                <a href="/{{$entry->category->page->slug}}/{{$entry->slug}}">
                    <img loading="lazy" src="/storage/entries/small/{{ $entry->image }}" alt="{{ $entry->name }}"
                        title="{{ $entry->name }}">
                    <h5 class="text-center pt-2 pb-0 mb-0">
                        {{ $entry->name }}
                    </h5>
                </a>
                <div class="districts px-5 pb-1 text-center">
                    @foreach ($entry->district as $district)
                        <small><a class="text-light"
                                href="{{ $entry->category->slug }}/p-{{ $district->slug }}">{{ $district->name }}</a></small>
                    @endforeach
                </div>
            </div>
        @endforeach
    </div>
</div>
