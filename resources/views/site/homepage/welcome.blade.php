<div class="welcome pb-2 mt-3 border-0">
    <h1 class="py-3 text-center">Η οργάνωση της βάπτισης γίνεται παιχνίδι!</h1>

    <div class="row align-items-start justify-content-between">
        <div class="col-md-12 pr-4 text-center">
            <p>Το <strong>Vaptisi Portal</strong> έρχεται να λύσει τα χέρια στους γονείς που οργανώνουν τη βάπτιση του
                παιδιού τους.</p>
            <p>Η ομάδα μας αναζητά τους κορυφαίους επαγγελματίες σε κάθε επαγγελματική κατηγορία, που με τα προϊόντα,
                τις υπηρεσίες και τις ιδέες τους θα προσφέρουν τις λύσεις που επιθυμεί κάθε γονιός για τη βάπτιση του
                παιδιού του.</p>
            <p>Στο Vaptisi Portal μπορείτε να οργανώσετε μία βάπτιση που όλοι θα θυμούνται για καιρό, με τον πιο εύκολο
                και γρήγορο τρόπο.</p>
            <p>Επιλέξτε την επαγγελματική κατηγορία που σας ενδιαφέρει και ξεκινήστε!</p>
        </div>
        
        {{-- <div class="ol-md-4">
            <a href="https://www.status-ek.gr/gamos-baptise-m-e-c" target="_blank"><img src="{{ asset('images/banners/gamos-vaptisi.png') }}" alt="Έκθεση Γάμος - Βάπτιση, MEC Παιανίας"></a>
        </div> --}}

    </div>

</div>
