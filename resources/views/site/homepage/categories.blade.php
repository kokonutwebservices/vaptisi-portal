<div class="categories d-md-flex flex-wrap">

    @foreach ($parentCategories as $parentCategory)
        <div class="{{$parentCategory->id === 1 ? 'first' : 'second'}}">
            <h2 class="ml-md-2 p-0 p-md-2 my-3 col-md-12 ">{{$parentCategory->name}}</h2>
            <div class="d-flex flex-wrap mb-5">
                @foreach ($categories->where('parent_id', $parentCategory->id) as $category)
                    <div class="col-md-3 p-0">
                        <div class="category-item m-md-2">
                            <a href="/{{$category->slug}}">
                            <img loading="lazy" src="{{asset('storage/categories/'.$category->image)}}" alt="{{$category->name}}" title="{{$category->name}}">
                            <h5 class="text-center py-2 ">{{$category->name}}</h5>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endforeach      

</div>
