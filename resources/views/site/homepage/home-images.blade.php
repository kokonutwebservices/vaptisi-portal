<div class="d-flex flex-wrap justify-content-center homeImages">
    @foreach ($homeImages as $image)

    <a href="/{{$image->album->entry->category->slug}}/{{$image->album->entry->slug}}">
        <img src="/storage/uploads/{{$image->album->entry->id}}/{{$image->album_id}}/small/{{$image->image}}" alt="" title="{{$image->album->entry->name}} - {{$image->album->entry->category->name}}" class="homeImage m-1">
    </a>  
    @endforeach
</div>
