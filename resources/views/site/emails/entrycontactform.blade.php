@component('mail::message')
# Vaptisi Portal: Έχετε μήνυμα εκδήλωσης ενδιαφέροντος

Το παρακάτω μήνυμα προέρχεται από τη φόρμα επικοινωνίας που υπάρχει στην καταχώρησή σας στο <a href="https://www.vaptisiportal.gr">Vaptisi Portal</a>, στην κατηγορία {{$category->name}}.

- Όνομα: {{$message->name}}
- Τηλέφωνο: <a href="tel: {{$message->phone}}">{{$message->phone}}</a>
- Email: <a href="mailto: {{$message->email}}">{{$message->email}}</a>

## Μήνυμα:

{{$message->message}}

@component('mail::button', ['url' => "mailto:$message->email"])
Απαντήστε εδώ
@endcomponent

Με εκτίμηση,<br>
Η ομάδα του Vaptisi Portal
@endcomponent
