@component('mail::message')
# Η καταχώρηση {{$entry->name}} έληξε στις {{$entry->end_at}} και απενεργοποιήθηκε

Στοιχεία πελάτη:

- Πελάτης: {{$entry->client->name}}
- Email: {{$entry->client->email}}
- Τηλ: {{$entry->shops[0]->tel}}


@endcomponent
