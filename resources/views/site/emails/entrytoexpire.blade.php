@component('mail::message')
# Η καταχώρηση {{$entry->name}} λήγει στις {{$entry->end_at}}

Στοιχεία πελάτη:

- Πελάτης: {{$entry->client->name}}
- Email: {{$entry->client->email}}
- Τηλ: {{$entry->shops[0]->tel}}
@endcomponent
