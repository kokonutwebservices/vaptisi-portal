@component('mail::message')
# Vaptisi Portal: Request Προβολής

- Επωνυμία: {{$request->title}}
- Όνομα: {{$request->name}}
- Τηλέφωνο: <a href="tel: {{$request->phone}}">{{$request->phone}}</a>
- Email: <a href="mailto: {{$request->email}}">{{$request->email}}</a>

## Μήνυμα:

{{$request->message}}


Με εκτίμηση,<br>
Η ομάδα του Vaptisi Portal
@endcomponent
