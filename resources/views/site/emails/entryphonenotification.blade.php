@component('mail::message')
# Vaptisi Portal: Ειδοποίηση Τηλεφωνικής επικοινωνίας

Ο πελάτης {{$shop->basicentry->name}} είχε τηλεφώνημα.


Με εκτίμηση,<br>
Η ομάδα του Vaptisi Portal
@endcomponent
