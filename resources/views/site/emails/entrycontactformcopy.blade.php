@component('mail::message')
# Vaptisi Portal: Η καταχώρηση {{$entry->name}} στην κατηγορία {{$category->name}} είχε μήνυμα με τα παρακάτω στοιχεία:

- Όνομα: {{$message->name}}
- Τηλέφωνο: <a href="tel: {{$message->phone}}">{{$message->phone}}</a>
- Email: <a href="mailto: {{$message->email}}">{{$message->email}}</a>

## Μήνυμα:

{{$message->message}}

@component('mail::button', ['url' => 'mailto:'. $entry->client->email])
Προωθήστε το εδώ
@endcomponent

Με εκτίμηση,<br>
Η ομάδα του Vaptisi Portal
@endcomponent
