@extends('site.layouts.master')

@section('metatags')
    <title> {{ $entry->metatitle }} </title>
    <meta name="description" content="{{ $entry->metadescription }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://www.vaptisiportal.gr/{{ $entry->category->slug }}/{{ $entry->slug }}">
    <meta property="og:title" content="{{ $entry->metatitle }}">
    <meta property="og:description" content="{{ $entry->metadescription }}">
    <meta property="og:image" content="https://www.vaptisiportal.gr/storage/entries/{{ $entry->image }}">
    <link rel="canonical" href="https://www.vaptisiportal.gr/{{ $entry->category->slug }}/{{ $entry->slug }}">
@endsection

@section('stylesheets')
    <link rel="stylesheet" href="/css/photoswipe.css">
    <link rel="stylesheet" href="/css/default-skin/default-skin.css">
@endsection

@section('content')
    <div class="entry {{ $entry->category->parent_id == 1 ? 'first' : 'second' }}">
        <div class="container-fluid p-0">

            {{-- Breadcrumbs --}}
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb py-2">
                    <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="/{{ $entry->category->slug }}">{{ $entry->category->name }}</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">{{ $entry->name }}</li>
                </ol>
            </nav>
            {{-- End Breadcrumbs --}}
        </div>

        <div class="container-fluid p-0 pb-2 border-bottom">
            <h1 class="text-center mb-4">{{ $entry->name }}</h1>
            <div class="row">
                <div class="col-md col-sm-12">
                    <img class="w-100 main-image" src="/storage/entries/{{ $entry->image }}" alt="{{ $entry->name }}">
                </div>
                <div class="col">
                    <div class="profile ml-md-5 pr-md-4 px-xs-5 px-3">
                        {!! $entry->profile !!}
                    </div>
                </div>

            </div>

        </div>

        <div class="container mt-4 mb-3">

            <div class="row">
                <div class="col-md col-sm-12 contact-info">
                    {{-- @include('site.entry.contact-info') --}}
                    <livewire:site.contact-info :entry="$entry" />
                </div>
                <div class="col contact-form">
                    @include('site.entry.contact-form')
                </div>
            </div>





            <div class="row photos">
                @include('site.entry.photos')
            </div>

            <div class="contact-form mt-3">
                <a class="btn d-block mx-auto" href="#contact-form">Contact Us</a>
            </div>
        </div>

        <div class="container-fluid p-0 d-md-none">

            {{-- Breadcrumbs --}}
            <nav aria-label="breadcrumb" class="text-center my-3">
                <a href="/{{ $entry->category->slug }}"><i class="fas fa-undo-alt"></i> {{ $entry->category->name }}</a>
            </nav>
            {{-- End Breadcrumbs --}}
        </div>
    </div>
@endsection

@section('photoswipe')
    <script src="/js/photoswipe.min.js"></script>
    <script src="/js/photoswipe-ui-default.min.js"></script>
@endsection
