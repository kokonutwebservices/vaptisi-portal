@extends('site.layouts.master')

@section('metatags')

<title>{{$infopage->metatitle}}</title>
<meta name="description" content="{{$infopage->metadescription}}">
    
@endsection

@section('content')

<div class="container">
    <h1 class="mt-5">{{$infopage->title}}</h1>

    @if ($infopage->image)
        <img class="img-fluid" src="/storage/infopages/{{$infopage->image}}" alt="{{$infopage->alt}}" title="{{$infopage->alt}}">
    @endif

    <div class="my-3">
        {!!$infopage->body!!}
    </div>

    @if (Request::is('advertise*'))
        @include('site.infopages.request')
    @endif
</div>

    
@endsection