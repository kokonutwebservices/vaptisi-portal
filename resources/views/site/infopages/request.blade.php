<div class="col-md-6 my-5 request">
    <h3 class="mt- mb-3">Φόρμα Επικοινωνίας</h3>
    <form action="/contact" method="POST">
        @csrf
    
    <div class="form-group">
      <input type="text" class="form-control" name="title" id="title" aria-describedby="helpId" placeholder="Επωνυμία Επιχείρησης" value="{{old('title')}}">
      @error('title')
          <small id="helpId" class="form-text text-danger">{{$message}}</small>  
      @enderror
    </div>
    
    <div class="form-group">
      <input type="text" class="form-control" name="name" id="name" aria-describedby="helpId" placeholder="Όνομα Υπευθύνου" value="{{old('name')}}">
      @error('name')
          <small id="helpId" class="form-text text-danger">{{$message}}</small>  
      @enderror
    </div>
    
    <div class="form-group">
      <input type="text" class="form-control" name="phone" id="phone" aria-describedby="helpId" placeholder="Τηλέφωνο" value="{{old('phone')}}">
      @error('phone')
          <small id="helpId" class="form-text text-danger">{{$message}}</small>  
      @enderror
    </div>
    
    <div class="form-group">
      <input type="text" class="form-control" name="email" id="email" aria-describedby="helpId" placeholder="Email" value="{{old('email')}}">
      @error('email')
          <small id="helpId" class="form-text text-danger">{{$message}}</small>  
      @enderror
    </div>
    
    <div class="form-group">
      <input type="text" class="form-control" name="website" id="website" aria-describedby="helpId" placeholder="Website" value="{{old('website')}}">
    </div>
    
    <div class="form-group">
      <textarea class="form-control" name="message" id="message" rows="3" placeholder="Το μήνυμά σας" >{{old('message')}}</textarea>
      @error('message')
          <small id="helpId" class="form-text text-danger">{{$message}}</small>  
      @enderror
    </div>

    <div class="form-check form-check-inline">
        
        <label class="form-check-label text-muted">
            <input class="form-check-input" type="checkbox" name="terms" id="terms" value="checkedValue"> <a href="/oroi-xrisis"> Συμφωνώ με τους όρους χρήσης </a>
        </label>    
    </div>
    @error('terms')
        <small id="helpId" class="form-text text-danger">{{$message}}</small>
    @enderror

    <button type="submit" class="btn btn-mint d-block mx-auto mt-4">Αποστολή</button>
    
    </form>
    <div class="mt-3">
        @if(session()->has('message'))
          <div class="alert alert-success">
              {{ session()->get('message') }}
          </div>
        @endif
    </div>
