<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>https://www.vaptisiportal.gr/</loc>
        {{-- <lastmod>{{ $categories->pluck('created_at')->max()->tz('UTC')->toAtomString() }}</lastmod> --}}
        <changefreq>weekly</changefreq>
        <priority>1</priority>
    </url>

    @foreach ($categories as $category)
        @if($category->entries)
        <loc>https://www.vaptisiportal.gr/{{$category->slug}}</loc>
        <lastmod>{{ $categories->pluck('created_at')->max()->tz('UTC')->toAtomString() }}</lastmod>
        <changefreq>weekly</changefreq>
        <priority>1</priority>
        @endif

        @foreach ($category->entries as $entry)
            <loc>https://www.vaptisiportal.gr/{{$category->slug}}/{{$entry->slug}}</loc>
            <changefreq>monthly</changefreq>
            <priority>1</priority>
        @endforeach

        @foreach ($districts as $district)
            @if($district->entries->where('category_id', $category->id)->count())
            <loc>https://www.vaptisiportal.gr/{{$category->slug}}/p-{{$district->slug}}</loc>
   
            <changefreq>monthly</changefreq>
            <priority>.8</priority>
            @endif
        @endforeach

    @endforeach
    
</urlset>