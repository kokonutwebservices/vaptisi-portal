<div>
    <h4 class="mt-4">Ημερομηνίες</h4>
       <p>Δημιουργία Καταχώρησης: {{ Carbon\Carbon::parse($entry->created_at)->toDateTimeString() }}</p>
    
       <div class="form-row">
           <div class="col-md-4">
                <label for="start">Έναρξη Καταχώρησης</label>
                <input type="" name="start_at" id="start" class="form-control" wire:model="start">
           </div>
           <div class="col-md-4">
                <label for="end">Λήξη Καταχώρησης</label>
           <input type="" name="end_at" id="end" class="form-control" wire:model="end">
           </div>
           <div class="col-md-3">
                <p class="text-center"><strong>Απενεργοποίηση</strong></p>
                <p class="text-center">{{Carbon\Carbon::parse($entry->start_at)->addYear()->addMonth()->format('d/m/Y, h:i:s A')}}</p>
           </div>
           <div class="col-md-12">
               <button class="btn badge-success" wire:click.prevent="renew">Ανανέωση</button>
           </div>
       </div>
</div>
