<div>
    <a href="/admin/entries/create" class="btn btn-primary my-3">Προσθήκη Καταχώρησης</a>

    <div class="float-right">
                    
        <input type="text" wire:model="searchTerm" placeholder="Αναζήτηση">
        <button
            wire:click="clearSearch"
        >
            x
        </button>
            
    </div>
    
    
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Όνομα Καταχώρησης</th>
                <th scope="col">Κατηγορία</th>
                <th scope="col">Περιφέρειες</th>
                <th scope="col">Ημ. Λήξης</th>
                <th scope="col">Διαγραφή</th>
            </tr>
        </thead>
        @foreach ($entries as $entry)
            <tr>
                <th scope="col">{{$entry->id}}</th>
                <td><a class="text-info" href="/admin/entries/{{$entry->id}}/edit">{{$entry->name}}</a></td>
                <td>{{$entry->category->name}}</td>
                <td>
                    <ul class="list-inline">
                    @foreach ($entry->district as $district)
                        <li class="list-inline-item badge badge-secondary">{{$district->name}}</li>
                    @endforeach
                    </ul>
                    
                </td>
                <td>{{$entry->end_at}}</td>
                <td>
                    <form id="delete" action="/admin/entries/{{$entry->id}}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="badge badge-danger">Διαγραφή</button>
                    </form>
                </td>
            </tr>
        @endforeach
        
    </table>
</div>
