<div id="contact-form" class="contact-form-body">
    <form wire:submit.prevent='submitForm' action="" method="POST">
      @csrf
        <div class="form-group form-name">
          <label for="name">ΟΝΟΜΑ</label>
          <input 
            wire:model="name"
            type="text"
            class="form-control" name="name" id="name" aria-describedby="helpId" 
            placeholder="" value="{{ old('name') }}"
            >
            @error('name')
                <small class="form-text text-danger">{{$message}}</small>
            @enderror
        
        </div>
        <div class="form-group form-email">
          <label for="email">EMAIL</label>
          <input 
            wire:model="email"
            type="text"
            class="form-control" name="email" id="email" aria-describedby="helpId"
            placeholder="" value="{{ old('email') }}"
            >
            @error('email')
              <small class="form-text text-danger">{{$message}}</small>            
            @enderror
        </div>
        <div class="form-group form-phone">
          <label for="phone">ΤΗΛΕΦΩΝΟ</label>
          <input 
            wire:model="phone"
            type="text"
            class="form-control" name="phone" id="phone" aria-describedby="helpId" 
            placeholder="" value="{{ old('phone') }}"
            >
            @error('phone')
              <small class="form-text text-danger">{{$message}}</small>          
            @enderror
        </div>
  
        <div class="form-group form-message">
          <label for="message">ΜΗΝΥΜΑ</label>
          <textarea
            wire:model="message"
            class="form-control" 
            name="message" 
            id="message" 
            rows="3" 
            placeholder="Σας είδαμε στο Vaptisi Portal και..."
            >{{old('message')}}</textarea>
        </div>
        @error('message')
            <small class="form-text text-danger">{{$message}}</small>
        @enderror
  
        <div class="d-flex justify-content-between mt-4">
          <div class="form-check form-check-inline">
            <label class="form-check-label">
              <input 
                wire:model="terms"
                class="form-check-input" 
                type="checkbox" 
                name="terms" 
                id="terms" 
                value="checkedValue"> <a href="/oroi-xrisis" target="blank">Αποδέχομαι τους όρους</a>
            </label>
            <div class="clearfix"></div>
            @error('terms')
                <span class="text-danger ml-3">Είναι απαραίτητο να αποδεχτείτε τους όρους</span>
            @enderror
          </div>
  
          <div class="">
            <button type="submit" class="btn  d-block m-auto">Αποστολή</button>
          </div>
        </div>
    </form>
  
    @if ($successMessage)
    <div id="alert" class="alert alert-success alert-dismissible fade show mt-3" role="alert">
        {{$successMessage}}
        <button
            wire:click="$set('successMessage', NULL)"
            type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    @endif
    
  </div>

