<div x-data="notifications()" class="contact">
    <h3>ΣΤΟΙΧΕΙΑ ΕΠΙΚΟΙΝΩΝΙΑΣ</h3>
    @foreach ($entry->shops as $key => $shop)
        <div class="mb-3">

            @if ($shop->city)
                <div class="entry-city py-3">
                    <i class="far fa-building"></i> {{ $shop->city }}
                </div>
            @endif
            @if ($shop->address)
                <div class="entry-address py-3">
                    <i class="fas fa-map-marker-alt"></i> {{ $shop->address }}
                </div>
            @endif
            @if ($shop->tel)
                <div class="entry-tel py-3">
                    <i class="fas fa-phone"></i>
                    <a @click.prevent="triggerPhoneNotification({{$shop->id}}, {{str_replace(' ', '',$shop->tel)}})" class="text-body"
                        href="tel:+30{{ $shop->tel }}">{{ $shop->tel }}</a>
                </div>
            @endif
            @if ($shop->mob)
                <div class="entry-mobile py-3">
                    <i class="fas fa-mobile-alt"></i>
                    <a @click.prevent="triggerPhoneNotification({{$shop->id}}, {{str_replace(' ', '',$shop->mob)}})" class="text-body" href="tel:+30{{ $shop->mob }}">{{ $shop->mob }}</a>            
                </div>
            @endif

        </div>
    @endforeach

    <div class="">

        @if ($entry->website)
            <div class=" py-3">
                <a href="http://{{ $entry->website }}" target="_blank" rel="nofollow">{{ $entry->website }}</a>
            </div>
        @endif

        <div class="social d-flex ">
            @if ($entry->facebook)
                <div class="mx-2 py-1">
                    <a href="{{ App\Entry::cleanLink($entry->facebook) }}" target="_blank" rel="nofollow"><i
                            class="fab fa-facebook-f"></i></a>
                </div>
            @endif
            @if ($entry->instagram)
                <div class="mx-2 py-1">
                    <a href="{{ App\Entry::cleanLink($entry->instagram) }}" target="_blank" rel="nofollow"><i
                            class="fab fa-instagram"></i></a>
                </div>
            @endif
        </div>
    </div>

</div>
@section('scripts')
<script>

    function notifications() {
        return {
            triggerPhoneNotification(shop, url) {
                window.location = "tel:"+url;
                this.$wire.phoneNotification(shop);
            }
        }
    }
    
</script>
@endsection

