@extends('admin.layouts.master')

@section('content')

<h1 class="mt-5">{{$entry->name}}</h1>
<h2 class="mt-2">Επεξεργασία Album: {{$album->name}}</h2>
{{-- Στοιχεία Album --}}
<div class="mt-3">
  <a class="text-secondary" data-toggle="collapse" href="#titleForm" role="button" aria-expanded="false" aria-controls="collapseExample">
    Στοιχεία Album    <i class="far fa-edit"></i>
  </a>
</div>
<div class="collapse" id="titleForm">
<form action="/admin/entries/{{$entry->id}}/albums/{{$album->id}}" method="POST">
    @method('PUT')
    @csrf
    <div class="form-group">
      <label for="name">Τίτλος</label>
      <input type="text" class="form-control" name="name" id="" value="{{$album->name}}">
    </div>
    <div class="form-group">
    <label for="description">Περιγραφή</label>
      <textarea class="form-control tinyMCE" name="description" id="" rows="3" >{{ $album->description }}</textarea>    
    </div>

    <button type="submit" class="btn btn-success">Αποθήκευση</button>
    
</form>
</div>

{{-- Επιλογή εξωφύλλου --}}

@if (count($images) > 0)

  <div class="mt-3">
    <a href="#cover" data-toggle="collapse" role="button" class="{{!$album->cover ? 'text-danger' : 'text-secondary'}}">
      Εικόνα εξωφύλλου  <i class="far fa-edit"></i>
    </a>
  </div>

  <div class="collapse" id="cover">  

      <form action="/admin/entries/{{$entry->id}}/albums/{{$album->id}}/images/cover" method="POST">
        @csrf
        @foreach ($images as $key=>$image)
        <div class="form-check form-check-inline">
          <label class="form-check-label">
            <input class="form-check-input" type="radio" name="image" id="" value="{{$image->image}}" {{ $image->image === $image->album->cover ? 'checked': "" }}> {{$key+1}}
          </label>
        </div>
        @endforeach
        <div>
        <button type="submit" class="btn btn-primary mt-1">Επιλογή</button>
        </div>
      </form>
  </div>
@endif

{{-- Ανέβασμα Φωτογραφιών --}}
<div class="mt-3 mb-2">
  <a class="text-secondary" data-toggle="collapse" href="#images" role="button" aria-expanded="false" aria-controls="collapseExample">
    Φωτογραφίες    <i class="far fa-edit"></i>
  </a>
</div>

<div class="text-secondary my-2">
  Το album διαθέτει {{count($images) == 1 ? count($images) . ' εικόνα' : count($images).' εικόνες' }} και μπορείτε να προσθέσετε μέχρι {{$avail}} ακόμη.
</div>

{{--  Πλαίσιο σφαλμάτων  --}}
@include('admin.layouts.errors') 
@if (session('message'))
    <div class="alert alert-success" id="successMessage">
        {{ session('message') }}
    </div>
@endif

<div class="collapse show" id="images">

  <ul id="imageSortable" class="d-flex flex-row flex-wrap justify-content-start p-0">
    @foreach ($images->sortBy('order') as $key=>$image) 
        <div class="d-flex flex-column mr-2" id="{{$entry->id}}-{{$album->id}}-{{$image->id}}">          
          <img src="/storage/uploads/{{$entry->id}}/{{$album->id}}/{{$image->image}}" alt="" style="width:200px"  class="img-thumbnail d-block mx-auto">
            <div class="d-flex flex-row">
              <a href="" class="btn btn-light">{{$image->order}}.</a>
              <a class="btn btn-light mx-2" data-toggle="collapse" href="#comment-{{$image->id}}"><i class="far fa-comment"></i></a>                
              <a class="btn btn-light mx-2" data-toggle="collapse" href="#move-{{$image->id}}"><i class="fas fa-suitcase"></i></a>
              {{-- Διαγραφή Εικόνας --}}
              <a class="btn btn-light mx-2" id="delete" href="/admin/entries/{{$entry->id}}/albums/{{$album->id}}/{{$image->id}}/delete"><i class="far fa-trash-alt"></i></a>
              {{-- Tags --}}
              <a class="btn btn-light mx-2" data-toggle="collapse" href="#tags-{{$image->id}}"><i class="fas fa-tags"></i></a>
            </div>            
            
          <div class="collapse mt-2" id="comment-{{$image->id}}">

            {{-- Περιγραφή Εικόνας --}}
            
            <form action="/admin/entries/{{$entry->id}}/albums/{{$album->id}}/images/{{$image->id}}/description" method="POST">
              @csrf
              <div>
              <textarea name="description" id="" cols="30" rows="4">{{$image->description}}</textarea>
              </div>
              <input type="hidden" name="image" value="{{$image->id}}">
              <button type="submit" class="btn btn-primary">Αποθήκευση</button>
            </form>            
          </div>

          {{-- Μετακίνηση σε άλλο album --}}
          <div class="collapse mt-2" id="move-{{$image->id}}">

            @if ($image->image === $image->album->cover)
                <span class="text-danger">Το εξώφυλλο του album δεν μπορεί να μετακινηθεί</span>
            @else

            <form action="/admin/entries/{{$entry->id}}/albums/{{$album->id}}/{{$image->id}}/changealbum" method="POST">
            @csrf

            <div class="form-group">
              <select class="form-control" name="newAlbum" id="">

                @foreach ($entryAlbums as $entryAlbum)

                <option value="{{$entryAlbum->id}}" {{$entryAlbum->id === $album->id ? 'selected' : ''}} class="{{count($entryAlbum->images()->get()) == 24 ? 'text-danger' : ''}}">
                  {{$entryAlbum->name}} -  {{count($entryAlbum->images()->get())}} φωτογραφίες
                </option>  

                @endforeach                

              </select>

              <button type="submit" class="btn btn-primary mt-2">Μεταφορά</button>
            </div>

            </form>

            @endif

            
          </div>

          {{-- Tags --}}

          <div class="collapse mt-2" id="tags-{{$image->id}}">

            <form action="/admin/entries/{{$entry->id}}/albums/{{$album->id}}/{{$image->id}}/tags" method="POST">
              @csrf
              <select name="tags[]" multiple class="form-control" id="tags">
                @foreach ($tags as $tag)
                    <option value="{{$tag->id}}" class="{{ $tag->images()->exists() ? "text-success" : "text-default" }}" {{$tag->images()->exists() ? "selected" : ""}}>{{$tag->name}}</option>
                @endforeach
              </select>
              <button type="submit" class="btn btn-info mt-2">Αποθήκευση</button>

            </form>
          </div>

        </div>

    @endforeach
      </ul>

    <form action="/admin/entries/{{$entry->id}}/albums/{{$album->id}}/images" method="POST" enctype="multipart/form-data">
      @csrf
      <div class="form-group">
        <label for=""></label>
        <input type="file" multiple="multiple" class="form-control-file" name="image[]" id="" placeholder="" aria-describedby="fileHelpId">
      </div>

      <input type="hidden" name="available" value="{{$avail}}">

      <button type="submit" class="btn btn-primary"  {{$avail <= 0 ?'disabled':''}}>Upload</button>
      <a href="/admin/entries/{{$entry->id}}/albums/create" class="btn btn-success">Προσθήκη album</a>
      <a href="../" class="btn btn-secondary">Επιστροφή στα album</a>
      <a href="../../edit" class="btn btn-info">Επιστροφή στην καταχώρηση</a>
    </form>
 
</div>

    
@endsection