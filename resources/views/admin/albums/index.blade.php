@extends('admin.layouts.master')

@section('content')

<h1 class="my-5">Albums Καταχώρησης {{$entry->name}}</h1>

<ul id="albumSortable" class="list-group">
    @foreach ($albums as $album)
        <li id="{{$entry->id}}-{{$album->id}}" class="list-group-item" data-entry="{{$entry->id}}" data-album="{{$album->id}}">
            <div class="row">
                <div class="col-md-2">
                    @if ($album->cover)                    
                        <img src="/storage/uploads/{{$entry->id}}/{{$album->id}}/{{$album->cover}}" alt="" style="width: 100px;">
                    @else
                        <img src="/storage/default.png" style="width: 100px;">
                    @endif
                </div>
                <div class="col-md-9">
                    <h4>
                        <a href="/admin/entries/{{$entry->id}}/albums/{{$album->id}}/edit">{{$album->name}}</a>
                    </h4>
                    <p>{{count($album->images)}} Φωτογραφίες</p>
                </div>
                <div class="col-md-1" id="delete">
                    <form action="/admin/entries/{{$entry->id}}/albums/{{$album->id}}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button id="delete" type="submit" class="badge badge-danger">ΔΙΑΓΡΑΦΗ</button>
                    </form>
                </div>
                
            </div>
        </li>    
    @endforeach
</ul>

<a href="/admin/entries/{{$entry->id}}/albums/create" class="btn btn-primary mt-5">Δημιουργία Album</a>
<a href="/admin/entries/{{$entry->id}}/edit" class="btn btn-secondary mt-5">Επιστροφή στην καταχώρηση</a>

    
@endsection