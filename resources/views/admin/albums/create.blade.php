@extends('admin.layouts.master')

@section('content')

<h1 class="my-5">Δημιουργία Album</h1>

<form action="/admin/entries/{{$entry->id}}/albums" method="POST">
    @csrf
    <div class="form-group">
    <label for="name">Όνομα Album</label>
      <input type="text" class="form-control" name="name" id="" placeholder="">
    </div>

    <div class="form-group">
    <label for="description">Περιγραφή Album</label>
        <textarea name="description" id="" cols="30" rows="10" class="tinyMCE"></textarea>
    </div>

    <button type="submit" class="btn btn-primary">Δημιουργία</button>
</form>
    
@endsection