@extends('admin.layouts.master')

@section('content')

<h1 class="mt-5">Επεξεργασία Περιφέρειας: {{$district->name}}</h1>

<form action="/admin/districts/{{$district->id}}" method="POST">
    @method('PATCH')
    @csrf
    <div class="form-group">
            <label for="name">Όνομα Περιφέρειας</label>
            <input type="text" class="form-control" name="name" id="name" value="{{$district->name}}" placeholder="Όνομα Περιφέρειας" required>
        </div>
        <div class="form-group">
            <label for="slug">Friendly URL</label>
            <input type="text" class="form-control" name="slug" id="slug" value="{{$district->slug}}" placeholder="friendly url" required>
        </div>
        <div class="form-group">
            <label for="metatitle">Meta Title</label>
            <input type="text" class="form-control" name="metatitle" id="metatitle" value="{{$district->metatitle}}" placeholder="Meta Title">
        </div>
        <div class="form-group">
            <label for="metadescription">Meta Description</label>
            <input type="text" class="form-control" name="metadescription" id="metadescription" value="{{$district->metadescription}}" placeholder="Meta Description">
        </div>
        <div class="form-group">
            <label for="metakeywords">Meta Keywords</label>
            <input type="text" class="form-control" name="metakeywords" id="metakeywords" value="{{$district->metakeywords}}" placeholder="Meta Keywords">
        </div>

        <div class="row">
            @include('admin.layouts.savebuttons')
        </form>
            <div class="col md-3">
                <a href="/admin/districts" class="btn btn-warning pull-right">Cancel</a>
            </div>
            <div class="col-md-3">
                <form action="/admin/districts/{{$district->id}}" method="POST" id="delete">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>
    </div>


@include('admin.layouts.errors')

@endsection