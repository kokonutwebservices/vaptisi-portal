@extends('admin.layouts.master')

@section('content')

    <h1 class="mt-5">Προσθήκη Περιφέρειας</h1>

    <form action="/admin/districts" method="POST">
        @csrf

        <div class="form-group">
            <label for="name">Όνομα Περιφέρειας</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Όνομα Περιφέρειας" required>
        </div>
        <div class="form-group">
            <label for="slug">Friendly URL</label>
            <input type="text" class="form-control" name="slug" id="slug" placeholder="friendly url" required>
        </div>
        <div class="form-group">
            <label for="metatitle">Meta Title</label>
            <input type="text" class="form-control" name="metatitle" id="metatitle" placeholder="Meta Title">
        </div>
        <div class="form-group">
            <label for="metadescription">Meta Description</label>
            <input type="text" class="form-control" name="metadescription" id="metadescription" placeholder="Meta Description">
        </div>
        <div class="form-group">
            <label for="metakeywords">Meta Keyowords</label>
            <input type="text" class="form-control" name="metakeywords" id="metakeywords" placeholder="Meta Keywords">
        </div>

        <div class="row">
            @include('admin.layouts.savebuttons')
            <div class="col md-3">
                <a href="/admin/districts" class="btn btn-warning pull-right">Cancel</a>
            </div>
        </div>

    </form>

    @include('admin.layouts.errors')
    
@endsection