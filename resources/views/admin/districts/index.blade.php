@extends('admin.layouts.master')

@section('content')
    <h1 class="mt-5">Περιφέρειες</h1>
    <ul class="list-unstyled">
    @foreach ($districts as $district)
        <li>
            <a class="text-info" href="/admin/districts/{{$district->id}}/edit">{{ $district->name }}</a>
        </li>
    @endforeach
    </ul>
    <a href="/admin/districts/create" class="btn btn-primary">Προσθήκη Περιφέρειας</a>
@endsection