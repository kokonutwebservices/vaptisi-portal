<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="/admin"><img class="bg-light admin-logo" src="/storage/admin/logo.png" alt=""></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
       
    @auth
        
    

    <div class="collapse navbar-collapse" id="navbarsExample05">
      
      <ul class="navbar-nav col-md-12 justify-content-between">
        {{-- <li class="nav-item active">
          <a class="nav-link" href="/admin">Home <span class="sr-only">(current)</span></a>
        </li> --}}
        <li class="nav-item">
          <a class="nav-link {{Request::is('admin/districts*') ? 'text-light' : ''}}" href="/admin/districts">Περιφέρειες</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{Request::is('admin/clients*') ? 'text-light' : ''}}" href="/admin/clients">Πελάτες</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{Request::is('admin/categories*') ? 'text-light' : ''}}" href="/admin/categories">Κατηγορίες</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{Request::is('admin/pages*') ? 'text-light' : ''}}" href="/admin/pages">Σελίδες Κατηγοριών</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{Request::is('admin/entries*') ? 'text-light' : ''}}" href="/admin/entries">Καταχωρήσεις</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{Request::is('admin/tags*') ? 'text-light' : ''}}" href="/admin/tags">Tags</a>
        </li>
        <li class="nav-item">
          <a class="nav-link {{Request::is('admin/infopages*') ? 'text-light' : ''}}" href="/admin/infopages">InfoPages</a>
        </li>
        @can('view_messages')
          <li class="nav-item">
            <a class="nav-link {{Request::is('admin/messages*') ? 'text-light' : ''}}" href="/admin/messages">Μηνύματα</a>
          </li>
        @endcan
        @can('edit_requests')
          <li class="nav-item">
            <a class="nav-link {{Request::is('admin/requests*') ? 'text-light' : ''}}" href="/admin/requests">Requests</a>
          </li>
        @endcan  
        
        
        @can('edit_users')
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle {{Request::is('admin/users*') || Request::is('admin/roles*') || Request::is('admin/abilities*') ? 'text-light' : ''}}" href="#" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">User Mgm</a>
            <div class="dropdown-menu" aria-labelledby="dropdown05">
              <a class="dropdown-item" href="/admin/users">Users</a>
              <a class="dropdown-item" href="/admin/roles">Roles</a>
              <a class="dropdown-item" href="/admin/abilities">Abilities</a>
            </div>
          </li>
        @endcan
        
        <li class="nav-item pull-right">
          <a class="nav-link" href="/" target="_blank">FrontEnd</a>
        </li>

        <li class="nav-item">
          <form action="/logout" method="POST">
            @csrf
            <button type="submit" class="btn btn-dark mt-1">Logout</button>
          </form>
        </li>
      </ul>
      
      {{-- <form class="form-inline my-2 my-md-0">
        <input class="form-control" type="text" placeholder="Search">
      </form> --}}
    </div>
    @endauth
  </nav>