<!DOCTYPE html>
<html lang="el">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Vaptisi Portal Admin Panel</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">    

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <script src="https://kit.fontawesome.com/f8ee92d2c6.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.8.2/dist/alpine.min.js" defer></script>

    <!-- Styles -->
    @livewireStyles
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="d-flex flex-column h-100">
    <header>
        @include('admin.layouts.nav')
    </header>

    <section>
        <div class="container admin">
            @yield('content')
        </div>    
    </section>
    
    <footer class="bg-dark p-1 mt-5">
        <div class="container">
            <footer class="mt-5">
                <h4 class="text-light">Vaptisi Portal Admin Panel</h4>
                <p class="text-light">Gamos Portal Team - Kokonut Web Services</p>
            </footer>
        </div>
    </footer>


    <!-- Scripts -->
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=cisfuks53sirpyxf6z5uhhy0vk3y58eiruscfv3jdi8mji7x"></script>
        <script>
            tinymce.init({
            selector: '.tinyMCE',
            height: 200,
            theme: 'modern',		  
            entity_encoding:'raw',            
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
            ],
            toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
            image_advtab: true,
            templates: [
                { title: 'Test template 1', content: 'Test 1' },
                { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]		  
            });
        </script>
    @livewireScripts
    <script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>