@if ($errors->any())
<div class="bg-danger text-white mt-3">
    <ul>
        @foreach ($errors->all() as $error)

            <li>{{$error}}</li>
            
        @endforeach
    </ul>
</div>
@endif