@extends('admin.layouts.master')

@section('content')
    <div class="container">
        <h1 class="mt-5">Request Προβολής {{$contactRequest->title}}</h1>

        <div class="card mt-5">
            <div class="card-header">
                <h4 class="card-title">{{$contactRequest->title}}</h4>
                <p class="card-text">{{\Carbon\Carbon::parse($contactRequest->created_at)->format('d/m/Y, h:i:s')}}</p>
                <form action="{{route('admin.request.delete', $contactRequest)}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger float-right">DELETE</button>
                </form>
            </div>
            <div class="card-body">            
                <p class="card-text">Όνομα: {{$contactRequest->name}}</p>
                <p class="card-text">email: {{$contactRequest->email}}</p>
                <p class="card-text">Τηλ: {{$contactRequest->phone}}</p>
                <p class="card-text">Μήνυμα: {{$contactRequest->message}}</p>
            </div>
        </div>

        <a href="/admin/requests" class="btn btn-secondary float-right mt-3"><i class="fas fa-undo-alt"></i> Επιστροφή</a>
    </div>
@endsection