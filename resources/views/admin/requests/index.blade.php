@extends('admin.layouts.master')

@section('content')
    <div class="container">
        <h1 class="mt-5 mb-3">Requests Προβολής</h1>
        <span class="float-right mb-2">Σύνολο: {{count($contactRequests)}}</span>
        <table class="table table-hover table-bordered">
            <thead class="thead-inverse">
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Επωνυμία</th>
                    <th scope="col">Όνομα</th>
                    <th scope="col">Ημερομηνία</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($contactRequests as $message)
                    <tr>
                        <th scope="row">{{$message->id}}</th>
                        <td> <a href="/admin/requests/{{$message->id}}">{{$message->title}}</a> </td>
                        <td> {{$message->name}} </td>
                        <td> {{\Carbon\Carbon::parse($message->created_at)->format('d/m/Y, h:i:s')}} </td>
                        
                    </tr>
                    @endforeach
                    
                </tbody>
        </table>

    </div>
@endsection