@extends('admin.layouts.master')

@section('content')

<h1 class="my-5">Tags</h1>

<div class="d-flex flex-wrap justify-space-between my-5">
    @foreach ($tags as $tag)
       <div class="m-2 border border-info rounded p-2">{{$tag->name}}

        <form action="/admin/tags/{{$tag->id}}" method="POST" class="float-right px-2">
            @csrf
            @method("DELETE")
            <button type="submit" class="text-danger btn-sm"><i class="far fa-trash-alt"></i></button>
        </form>

    </div>
    @endforeach
</div>

<form action="/admin/tags" method="POST">
    @csrf
    <div class="input-group input-group-sm mb-3">
        <div class="input-group-prepend">
            <button type="submit" class="btn btn-primary inline">+</button>
        </div>
        <input type="text" name="name" placeholder="Προσθήκη tag" class="form-control"required>
      </div>
</form>

@include('admin.layouts.errors')

@endsection