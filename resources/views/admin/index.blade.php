@extends('admin.layouts.master')

@section('content')

<div class="mt-5">
    <div class="card">
        <div class="card-header">
          <h2>Πελάτες</h2>
        </div>
        <div class="card-body">
            <h5 class="card-title">Επιλέξτε το επόμενο βήμα</h5>
            <p class="card-text">Αυτή τη στιγμή υπάρχουν {{ count(\App\Client::all())}} πελάτες.</p>
            <a href="/admin/clients" class="btn btn-primary">Δείτε τους πελάτες</a>
            <a href="/admin/clients/create" class="btn btn-success">Δημιουργία νέου πελάτη</a>  
        </div>
    </div>
    <div class="card">
        <div class="card-header">
          <h2>Καταχωρήσεις</h2>
        </div>
        <div class="card-body">
            <h5 class="card-title">Επιλέξτε το επόμενο βήμα</h5>
            <p class="card-text">Αυτή τη στιγμή υπάρχουν {{ count(\App\Entry::all())}} καταχωρήσεις.</p>
            <a href="/admin/entries" class="btn btn-primary">Δείτε τις καταχωρήσεις</a>
            <a href="/admin/entries/create" class="btn btn-success">Δημιουργία νέας καταχώρησης</a>  
        </div>
    </div>
</div>
    
@endsection