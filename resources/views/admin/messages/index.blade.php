@extends('admin.layouts.master')

@section('content')

    <div class="container">

        <div class="py-4">
            <div class="card">
            
                    <div class="card-header"> Μηνύματα Πελατών <small>({{ count($messages) }})</small></div>
                    @foreach ($messages as $message)
                    <div class="card-body p-1" x-data="{show: false}" @click="show = !show">
                        <div class="list-group-item list-group-item-action flex-column align-items-start" >
                            <div class="d-flex w-100 justify-content-between">
            
                                <h4 class="mb-4">
                                    <small class="mr-3">ID: {{$message->id}} </small>
                                    {{$message->entry_name}}
                                </h4>
                                <span>{{$message->category->name}}</span>
                                <small class="text-muted">{{ \Carbon\Carbon::parse($message->created_at)->format('d/m/Y, h:i:s') }}</small>
                                @if (auth()->user()->can('delete_messages'))
            
                                    <form method="post" action="{{ route('messages.destroy', ['message' => $message->id]) }}">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="badge badge-danger">ΔΙΑΓΡΑΦΗ</button>
                                    </form>
                            @endif
                            </div>
                            @if (auth()->user()->can('view_messages'))
                            <div x-show="show">
                                <p class="mb-1">{{$message->name}}</p>
                                <p class="mb-1">{{$message->phone}}</p>
                                <p class="mb-1">{{$message->email}}</p>
                                <p class="mb-1">{{$message->message}}</p>
                            </div>
                            @endif
                         
                        </div>
                    </div>
                @endforeach
            
            </div>
        </div>
    </div>
    </div>

@endsection
