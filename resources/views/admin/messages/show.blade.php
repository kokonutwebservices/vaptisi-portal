@extends('admin.layouts.master')

@section('content')
    <div class="container">
        <h1 class="mt-5">Μήνυμα για την καταχώρηση: {{$message->entry->name}}</h1>

        <div class="card mt-5">
            <div class="card-header">
                <h4 class="card-title">Καταχώρηση: <a href="/admin/entries/{{$message->entry->id}}/edit">{{$message->entry->name}}</a></h4>
                <p class="card-text">{{\Carbon\Carbon::parse($message->created_at)->format('d/m/Y, h:i:s')}}</p>
            </div>
            <div class="card-body">            
                <p class="card-text">Όνομα: {{$message->name}}</p>
                <p class="card-text">email: {{$message->email}}</p>
                <p class="card-text">Τηλ: {{$message->phone}}</p>
                <p class="card-text">Μήνυμα: {{$message->message}}</p>
            </div>
        </div>
    </div>
    
    <form class="text-center mt-3" method="post" action="{{route('messages.destroy', ['message' => $message->id])}}">
        @csrf
        @method('DELETE')

        <button 
            type="submit"
            class="btn btn-danger"
        >
        ΔΙΑΓΡΑΦΗ
        </button>

    </form>
@endsection