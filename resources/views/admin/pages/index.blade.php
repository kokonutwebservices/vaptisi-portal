@extends('admin.layouts.master')

@section('content')

<h1 class="my-5">Σελίδες Κατηγοριών Vaptisi Portal</h1>

<div id="pagesSortable">
@foreach ($pages as $page)

    <div id="{{$page->id}}" class="py-1">
        <a class="text-info" href="/admin/pages/{{$page->id}}/edit">{{$page->title}}</a>
    </div>
    
@endforeach
</div>

<a class="btn btn-primary mt-5" href="/admin/pages/create">Δημιουργία Σελίδας</a>
    
@endsection