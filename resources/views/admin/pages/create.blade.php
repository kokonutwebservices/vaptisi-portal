@extends('admin.layouts.master')

@section('content')

<h1 class="mt-5">Δημιουργία Σελίδας Κατηγοριών</h1>

@include('admin.layouts.errors')

<form action="/admin/pages" method="POST">
@csrf

<div class="form-group">
  <label for="title">Τίτλος Σελίδας</label>
  <input type="text" class="form-control" name="title" id="title" placeholder="">
</div>

<div class="form-group">
    <label for="slug">slug</label>
    <input type="text" class="form-control" name="slug" id="slug" placeholder="">
</div>

<div class="form-group">
  <label for="category_id">Κατηγορία</label>
  <select class="form-control" name="category_id" id="category_id">
    <option>Επιλέξτε κατηγορία</option>
    
    @foreach ($categories as $category)

    <option value="{{$category->id}}">{{$category->name}}</option>
        
    @endforeach
   
  </select>
</div>

<div class="form-group">
  <label for="description">Περιγραφή</label>
  <textarea class="form-control tinyMCE" name="description" id="description" rows="3"></textarea>
</div>

<div class="form-group">
  <label for="subtitle">Υπότιτλος</label>
  <input type="text" class="form-control" name="subtitle" id="subtitle" placeholder="">
</div>

<div class="form-group">
    <label for="metatitle">Meta Title</label>
    <input type="text" class="form-control" name="metatitle" id="metatitle" placeholder="">
</div>

<div class="form-group">
    <label for="metadescription">Meta Description</label>
    <input type="text" class="form-control" name="metadescription" id="metadescription" placeholder="">
</div>

<div class="form-group">
    <label for="metakeywords">Meta Keywords</label>
    <input type="text" class="form-control" name="metakeywords" id="metakeywords" placeholder="">
</div>

<div class="row mt-4">
    @include('admin.layouts.savebuttons')
    <div class="col-md-3">
        <a href="/admin/pages" class="btn btn-warning">Cancel</a>
    </div>
</div>

</form>

@include('admin.layouts.errors')

@endsection