@extends('admin.layouts.master')

@section('content')

<h1 class="mt-5">
    Επεγξεργασία σελίδας: {{$page->title}}
</h1>

@include('admin.layouts.errors')

<form action="/admin/pages/{{$page->id}}" method="POST">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label for="title">Τίτλος Σελίδας</label>
        <input type="text" class="form-control" name="title" id="title" value="{{$page->title}}">
    </div>
    
    <div class="form-group">
        <label for="slug">slug</label>
        <input type="text" class="form-control" name="slug" id="slug" value="{{$page->slug}}">
    </div>

    <div class="form-group">
        <label for="category_id">Κατηγορία</label>
        <select class="form-control" name="category_id" id="category_id">
            
            @foreach ($categories as $category)
        
            <option value="{{$category->id}}" {{$page->category->id === $category->id ? "selected": ""}}>{{$category->name}}</option>
                
            @endforeach
            
        </select>
    </div>

    <div class="form-group">
        <label for="description">Περιγραφή</label>
    <textarea class="form-control tinyMCE" name="description" id="description" rows="3">{{$page->description}}</textarea>
    </div>

    <div class="form-group">
        <label for="subtitle">Υπότιτλος</label>
        <input type="text" class="form-control" name="subtitle" id="subtitle" value="{{$page->subtitle}}">
        </div>
        
        <div class="form-group">
            <label for="metatitle">Meta Title</label>
            <input type="text" class="form-control" name="metatitle" id="metatitle" value="{{$page->metatitle}}">
        </div>
        
        <div class="form-group">
            <label for="metadescription">Meta Description</label>
            <input type="text" class="form-control" name="metadescription" id="metadescription" value="{{$page->metadescription}}">
        </div>
        
        <div class="form-group">
            <label for="metakeywords">Meta Keywords</label>
            <input type="text" class="form-control" name="metakeywords" id="metakeywords" value="{{$page->metakeywords}}">
        </div>
        
        <div class="row mt-4">
        @include('admin.layouts.savebuttons')
        <div class="col-md-3">
            <a href="/admin/pages" class="btn btn-warning">Cancel</a>
        </div>

    </form>

        <div class="col-md-3" id="delete">
            <form action="/admin/pages/{{$page->id}}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </div>
    </div>

    

@include('admin.layouts.errors')
@endsection