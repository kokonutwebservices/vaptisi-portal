@extends('admin.layouts.master')

@section('content')

<div class="container">
    <h1 class="mt-5">Users</h1>

    <table class="table table-hover table-bordered">
        <thead class="thead-inverse">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Username</th>
                <th scope="col">Roles</th>
                <th scope="col">Ημερομηνία</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                <tr>
                    <th scope="row">{{$user->id}}</th>
                    <td> <a href="/admin/users/{{$user->id}}/edit">{{$user->name}}</a> </td>
                    <td> {{$user->roles->pluck('label')}} </td>
                    <td> {{\Carbon\Carbon::parse($user->created_at)->format('d/m/Y, h:i:s')}} </td>
                    
                </tr>
                @endforeach
                
            </tbody>
    </table>
    
    <a href="/admin/users/create" class="btn btn-primary float-right">Προσθήκη</a>
</div>
@endsection