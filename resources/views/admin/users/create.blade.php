@extends('admin.layouts.master')

@section('content')

<div class="container">
    <h1 class="mt-5">Προσθήκη Χρήστη</h1>

    <form class="col-md-6" action="/admin/users" method="POST">
    @csrf

    <div class="form-group">
      <label for="name">Όνομα</label>
      <input type="text" class="form-control" name="name" id="name" aria-describedby="helpId" value="{{old('name')}}">
      @error('name')
        <small id="helpId" class="form-text text-danger">{{$message}}</small>  
      @enderror
    </div>

    <div class="form-group">
        <label for="email">Email</label>
        <input type="text" class="form-control" name="email" id="email" aria-describedby="helpId" value="{{old('email')}}">
        @error('email')
            <small id="helpId" class="form-text text-danger">{{$message}}</small>            
        @enderror
    </div>
    
    <div class="form-group">
        <label for="password">Password</label>
        <input type="text" class="form-control" name="password" id="password" aria-describedby="helpId" value="{{old('password')}}">
        @error('password')
            <small id="helpId" class="form-text text-danger">{{$message}}</small>            
        @enderror
    </div>

    <div class="form-check my-4">
      <label class="form-check-label">
        @foreach ($roles as $role)
        <div class="my-2">
          <input type="checkbox" class="form-check-input py-3" name="roles[]" id="" value="{{$role->id}}">
        {{$role->label}}
        </div>        
        
        @endforeach
      </label>
    </div>
    @error('roles')
        <small id="helpId" class="form-text text-danger">{{$message}}</small>
    @enderror

    <button type="submit" class="btn btn-primary">Προσθήκη</button>
    <a href="/admin/users" class="btn btn-warning">Cancel</a>

    </form>

</div>
    
@endsection