@extends('admin.layouts.master')

@section('content')

<div class="container">
    <h1 class="mt-5">Roles</h1>

    <table class="table table-hover table-bordered">
        <thead class="thead-inverse">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Label</th>
                <th scope="col">Name</th>
                <th scope="col">Abilities</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($roles as $role)
                <tr>
                    <th scope="row">{{$role->id}}</th>
                    <td> <a href="/admin/roles/{{$role->id}}/edit">{{$role->label}} ({{count($role->users)}})</a> </td>
                    <td> {{$role->name}} </td>
                    <td> 
                        @foreach ($role->abilities as $ability)
                            <span class="badge badge-secondary">{{$ability->label}} </span>
                        @endforeach
                    </td>
                </tr>
                @endforeach                
            </tbody>
    </table>

    <a href="/admin/roles/create" class="btn btn-primary float-right">Create Role</a>
</div>
    
@endsection