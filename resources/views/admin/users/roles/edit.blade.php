@extends('admin.layouts.master')

@section('content')

<div class="container">
    <h1 class="mt-5">Edit User Role: {{$role->label}}</h1>

    <form action="/admin/roles/{{$role->id}}" method="POST">
        @csrf
        @method('PUT')

        <div class="form-row">
            <div class="col">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" class="form-control" value="{{$role->name}}">
                @error('name')
                    <small id="helpId" class="form-text text-danger">{{$message}}</small>  
                @enderror
            </div>
            <div class="col">
                <label for="label">Label</label>
                <input type="text" name="label" id="label" class="form-control" value="{{$role->label}}">
                @error('label')
                    <small id="helpId" class="form-text text-danger">{{$message}}</small>  
                @enderror
            </div>
        </div>

        <div class="form-check mt-4">
            <label class="form-check-label">
                <h4><u>Abilities</u></h4>
                <div class="py-1">
                    <input class="form-check-input" type="checkbox" id="checkAll"> <u>Check All</u>
                </div>
                @foreach ($abilities as $ability)
                    <div class="py-1">
                        <input type="checkbox" class="form-check-input" name="abilities[]" id="" value="{{$ability->id}}" {{$role->abilities->contains($ability->id) ? 'checked' : '' }} > {{$ability->label}}               
                    </div>
                @endforeach
              
            </label>
          </div>

          <div class="mt-4">
            <button type="submit" class="btn btn-primary mx-2">Update</button>
            <a href="/admin/roles" class="btn btn-warning mx-2">Cancel</a>
          </div>
          
        </form>
</div>

@endsection