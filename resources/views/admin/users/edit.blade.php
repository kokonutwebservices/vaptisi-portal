@extends('admin.layouts.master')

@section('content')

<div class="container">
    <h1 class="mt-5">Επεξεργασία Χρήστη {{$user->name}}</h1>

    <form class="col-md-6" action="/admin/users/{{$user->id}}" method="POST">
        @csrf
        @method('PUT')
    
        <div class="form-group">
          <label for="name">Όνομα</label>
          <input type="text" class="form-control" name="name" id="name" aria-describedby="helpId" value="{{$user->name}}">
          @error('name')
            <small id="helpId" class="form-text text-danger">{{$message}}</small>  
          @enderror
        </div>
    
        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" name="email" id="email" aria-describedby="helpId" value="{{$user->email}}">
            @error('email')
                <small id="helpId" class="form-text text-danger">{{$message}}</small>            
            @enderror
        </div>
        
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="password" id="password" aria-describedby="helpId" value="{{$user->password}}">
            @error('password')
                <small id="helpId" class="form-text text-danger">{{$message}}</small>            
            @enderror
        </div>
    
        <div class="form-check">
          <label class="form-check-label">
            <input type="checkbox" class="form-check-input" name="roles[]" id="" value="1" {{$user->roles->contains('1') ? 'checked' : ''}}>
            Super Admin
            <br>
            <input type="checkbox" class="form-check-input" name="roles[]" id="" value="2" {{$user->roles->contains('2') ? 'checked' : ''}}>
            Admin
          </label>
        </div>
        @error('roles')
            <small id="helpId" class="form-text text-danger">{{$message}}</small>
        @enderror
    
        

        <div class="d-flex">
          <button type="submit" class="btn btn-primary mr-3">Ενημέρωση</button>
        </form>
          <a href="/admin/users" class="btn btn-warning mr-3">Cancel</a>
          <form action="/admin/users/{{$user->id}}" method="POST">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger mr-3">Delete</button>
          </form>
        </div>

</div>

@endsection