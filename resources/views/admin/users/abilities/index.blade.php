@extends('admin.layouts.master')

@section('content')

<div class="container">
    <h1 class="mt-5">Abilities</h1>

    <div class="d-flex flex-wrap">
        @foreach ($abilities as $ability)
                <div class="d-flex mr-5 my-3">
                    <a class="btn btn-outline-secondary">{{$ability->label}} ({{$ability->name}})</a>
                    <form action="/admin/abilities/{{$ability->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="badge badge-danger">X</button>
                    </form>
                </div>        
            @endforeach
    </div>
   

    <form class="mt-5" action="/admin/abilities" method="POST">
        @csrf

        <div class="form-row">
            <div class="col">
                <label for="name">Name</label>
                <input type="text"
                    class="form-control" name="name" id="name" aria-describedby="helpId" placeholder="π.χ. edit_users" value="{{old('name')}}">
                    @error('name')
                        <small id="helpId" class="form-text text-danger">{{$message}}</small>                
                    @enderror
            </div>
            <div class="col">
                <label for="label">Label</label>
                <input type="text"
                class="form-control" name="label" id="label" aria-describedby="helpId" placeholder="π.χ. Edit Users" value="{{old('label')}}">
                @error('label')
                    <small id="helpId" class="form-text text-danger">{{$message}}</small>                
                @enderror
            </div>          
        </div>

        <button type="submit" class="btn btn-primary mt-3">Προσθήκη</button>

    </form>
</div>
    
    

@endsection