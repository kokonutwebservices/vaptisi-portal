@extends('admin.layouts.master')

@section('content')
    <div class="mt-5">
        <h1>Πελάτες Vaptisi Portal</h1>
        <span>{{count($clients)}} πελάτες</span>
        <div class="clearfix"></div>
        <a href="/admin/clients/create" class="btn btn-primary my-3">Προσθήκη Πελάτη</a>

        <div class="float-right">
            <form action="/admin/clients/search" method="POST">
                @csrf
                
                <input type="text" name="query" id="query" placeholder="Αναζήτηση">
                <button class="btn btn-primary" type="submit">Sumbit</button>
                <a class="btn btn-secondary" role="button" href="/admin/clients">Reset</a>
            </form>
        </div>

        <table class="table table-hover table-bordered">
            <thead class="thead-inverse">
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Όνομα Πελάτη</th>
                    <th scope="col">Email</th>
                    <th scope="col">Αριθμός Καταχωρήσεων</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($clients as $client)
                    <tr>
                        <th scope="row">{{$client->id}}</th>
                        <td><a class="text-info" href="/admin/clients/{{$client->id}}/edit">{{$client->name}}</a></td>
                        <td>{{$client->email}}</td>
                        <td class="text-center"> {{count($client->entries)}} 
                            @if (count($client->entries) == 0)
                            
                                <form id="delete" action="/admin/clients/{{$client->id}}" method="post">
                                    @method('DELETE')
                                    @csrf 
                                    <button type="submit" class="badge badge-danger">Διαγραφή</button>
                                </form>

                            @endif
                        </td>
                    </tr>
                    @endforeach
                    
                </tbody>
        </table>

        <div class="mt-2">
            <a class="btn btn-primary" href="/admin/clients/create">Προσθήκη Πελάτη</a>
        </div>
            
    </div>
    
@endsection