@extends('admin.layouts.master')

@section('content')
<div class="mt-5">
    <h1>Επεξεργασία Πελάτη: {{$client->name}}</h1>
    <div class="container">
        <form method="POST" action="/admin/clients/{{$client->id}}">
            
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="name">Όνομα Πελάτη</label>
                <div class="col-sm-1-12">
                    <input type="text" class="form-control" name="name" id="name" value="{{$client->name}}">
                </div>
            </div>
            <div class="form-group">
              <label for="email">email</label>
              <input type="text" name="email" id="email" class="form-control" value="{{$client->email}}">
            </div>
            <div class="form-group">
              <label for="password">password</label>
            <input type="password" name="password" id="" class="form-control" placeholder="" value="{{$client->password}}">
            </div>
            
            <div class="form-group row">
                @include('admin.layouts.savebuttons')
                <div class="col md-3">
                        <a href="/admin/clients" class="btn btn-warning pull-right">Cancel</a>
                </div>
 
        </form>
        <form method="POST" action="/admin/clients/{{$client->id}}" id="delete">
            @method('DELETE')
            @csrf
            <button type="submit" class="btn btn-danger" {{ count($client->entries) !=0 ? 'disabled' : '' }}>Διαγραφή</button>
        </form>
        </div>
    </div>

    @if (count($client->entries) )
    
    <div class="container mt-5">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Όνομα Καταχώρησης</th>
                    <th scope="col">Κατηγορία</th>
                    <th scope="col">Περιφέρειες</th>
                    <th scope="col">Ημ. Λήξης</th>
                    <th scope="col">Διαγραφή</th>
                </tr>
            </thead>
            @foreach ($client->entries as $entry)
                <tr>
                    <th scope="col">{{$entry->id}}</th>
                    <td><a href="/admin/entries/{{$entry->id}}/edit">{{$entry->name}}</a></td>
                    <td>{{$entry->category->name}}</td>
                    <td>
                        <ul class="list-inline">
                        @foreach ($entry->district as $district)
                            <li class="list-inline-item badge badge-secondary">{{$district->name}}</li>
                        @endforeach
                        </ul>
                        
                    </td>
                    <td>{{$entry->end_at}}</td>
                    <td>
                        <form id="delete" action="/admin/entries/{{$entry->id}}" method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="badge badge-danger">Διαγραφή</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            
        </table>
        
    </div>
    @endif
</div>

@endsection