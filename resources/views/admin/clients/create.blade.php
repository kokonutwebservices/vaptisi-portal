@extends('admin.layouts.master')

@section('content')
<div class="mt-5">
    <h1>Δημιουργία Νέου Πελάτη</h1>
    <div class="container">
        <form method="POST" action="/admin/clients">
            @csrf
            <div class="form-group">
                <label for="name">Όνομα Πελάτη</label>
                <div class="col-sm-1-12">
                    <input type="text" class="form-control" name="name" id="name" placeholder="">
                </div>
            </div>
            <div class="form-group">
              <label for="email">email</label>
              <input type="text" name="email" id="email" class="form-control" placeholder="">
            </div>
            <div class="form-group">
              <label for="password">password</label>
            <input type="text" name="password" id="" class="form-control" placeholder="" value="{{Str::random(12)}}">
            </div>
            
            <div class="form-group row">
                @include('admin.layouts.savebuttons')
                <div class="col md-3">
                        <a href="/admin/clients" class="btn btn-warning pull-right">Cancel</a>
                </div>
            </div>
        </form>
        @include('admin.layouts.errors')
    </div>
</div>

@endsection