@extends('admin.layouts.master')

@section('content')
<div class="mt-5">
    <h1>Επεξεργασία Κατηγορίας: {{$category->name}}</h1>

    <form method="POST" action="/admin/categories/{{$category->id}}" enctype="multipart/form-data">
        @method('PUT')
        @csrf

        <div class="form-group">
              <label for="name">Όνομα Κατηγορίας</label>
              <input type="text" name="name" id="name" class="form-control" value="{{$category->name}}">
            </div>

            <div class="form-group">
              <label for="slug">Slug</label>
              <input type="text" name="slug" id="slug" class="form-control" value="{{$category->slug}}">
            </div>

            <div class="form-group">
              <label for="description">Κείμενο Περιγραφής Κατηγορίας</label>
              <textarea class="form-control tinyMCE" name="description" id="description" rows="3" value="{{$category->description}}">{{$category->description}}</textarea>
            </div>
            

            <div class="form-group">
              <label for="parent_id">Μητρική Κατηγορία</label>
              <select class="form-control" name="parent_id" id="parent_id">
                <option value="0">Καμία. Αυτή είναι η μητρική</option>
                @foreach ($parents as $parent)
                    @if($parent->id == $category->parent_id)
                    <option value="{{$parent->id}}" selected>{{$parent->name}}</option> 
                    @else 
                    <option value="{{$parent->id}}">{{$parent->name}}</option> 
                    @endif
                                     
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <label for="order"></label>
              <input type="number" min="1" name="order" id="order" class="form-control" placeholder="Σειρά εμφάνισης" value="{{$category->order}}">
            </div>

            <div class="custom-file">
              <label for="image" class="">Εικόνα Κατηγορίας
                  <div class="clearfix"></div>
                  @if ($category->image)
                    <img src="/storage/categories/{{$category->image}}" alt="" style="width:300px;">                      
                  @endif
                  <div class="clearfix"></div>
                  <span class="btn btn-secondary mt-1 p-1">Αναζήτηση</button>
              </label>
              <input type="file" class="custom-file-input" name="image" id="image" placeholder="" >
             
            </div>

            <div class="form-group">
              <label for="metatitle">Meta Title</label>
              <input type="text" class="form-control" name="metatitle" id="metatitle" value="{{$category->metatitle}}">
              @error('metatitle')
                <small id="helpId" class="form-text text-danger">{{$message}}</small>                  
              @enderror
            </div>

            <div class="form-group">
              <label for="metadescription">Meta Description</label>
              <input type="text" class="form-control" name="metadescription" id="metadescription" value="{{$category->metadescription}}">
              @error('metadescription')
                <small id="helpId" class="form-text text-danger">{{$message}}</small>                  
              @enderror
            </div>

            <div class="form-group">
              <label for="metakeywords">Meta Keywords</label>
              <input type="text" class="form-control" name="metakeywords" id="metakeywords" value="{{$category->metakeywords}}">
              @error('metakeywords')
                <small id="helpId" class="form-text text-danger">{{$message}}</small>                  
              @enderror
            </div>

            <div class="row">
                @include('admin.layouts.savebuttons')
                <div class="col md-3">
                  <a href="/admin/categories" class="btn btn-warning pull-right">Cancel</a>
                </div>
                <div class="col-md-2">
                </form>
                {{-- Κλείσιμο φόρμας Update --}}
                {{-- Φόρμα Delete --}}
                    <form action="/admin/categories/{{$category->id}}" method="POST" id="delete">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>
            </div>
            
@include('admin.layouts.errors')

</div>

    
@endsection