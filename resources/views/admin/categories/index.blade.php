@extends('admin.layouts.master')

@section('content')

<div class="mt-5">
    <h1>Επαγγελματικές Κατηγορίες Vaptisi Portal</h1>
    <ul id="categorySortable" class="list-unstyled">
        @foreach ($categories->where('parent_id', 0)->sortBy('order') as $parentCategory)
        <li id="{{$parentCategory->id}}">
            <a class="text-info" href="{{$parentCategory->adminPath()}}/edit">
                {{$parentCategory->name}}
            </a>
        
            <ul id="subcategorySortable" class="list-group list-unstyled pl-4 my-2">
                @foreach ($categories->where('parent_id', $parentCategory->id)->sortBy('order') as $category)
                    <li id="{{$category->id}}">
                        <a class="text-body" href="{{$category->adminPath()}}/edit">{{$category->name}}</a>
                    </li>
                @endforeach
            </ul>
        </li>
        @endforeach
    </ul>

    <div>
        <a href="/admin/categories/create" class="btn btn-primary mt-3">Προσθήκη Κατηγορίας</a>
    </div>
</div>    
@endsection