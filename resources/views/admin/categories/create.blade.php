@extends('admin.layouts.master')

@section('content')
    <div class="mt-5">
        <h1>Προσθήκη Νέας Κατηγορίας</h1>
        <form action="/admin/categories" method="POST" enctype="multipart/form-data">
          @csrf

            <div class="form-group">
              <label for="name">Όνομα Κατηγορίας</label>
              <input type="text" name="name" id="name" class="form-control" placeholder="">
              @error('name')
                <small id="helpId" class="form-text text-danger">{{$message}}</small>                  
              @enderror
            </div>

            <div class="form-group">
              <label for="slug">Slug</label>
              <input type="text" name="slug" id="slug" class="form-control" placeholder="">
              @error('slug')
                <small id="helpId" class="form-text text-danger">{{$message}}</small>                  
              @enderror
            </div>

            <div class="form-group">
              <label for="description">Κείμενο Περιγραφής Κατηγορίας</label>
              <textarea class="form-control tinyMCE" name="description" id="description" rows="3"></textarea>
            </div>

            <div class="form-group">
              <label for="parent_id">Μητρική Κατηγορία</label>
              <select class="form-control" name="parent_id" id="parent_id">
                <option value="0">Καμία. Αυτή είναι η μητρική</option>
                @foreach ($parents as $parent)
                  <option value="{{$parent->id}}">{{$parent->name}}</option>                    
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <label for="image">Εικόνα Κατηγορίας</label>
              <input type="file" class="form-control-file" name="image" id="image" placeholder="">
              @error('image')
                <small id="helpId" class="form-text text-danger">{{$message}}</small>                  
              @enderror
            </div>

            <div class="form-group">
              <label for="metatitle">Meta Title</label>
              <input type="text" class="form-control" name="metatitle" id="metatitle" aria-describedby="helpId" placeholder="">
              @error('metatitle')
                <small id="helpId" class="form-text text-danger">{{$message}}</small>                  
              @enderror
            </div>

            <div class="form-group">
              <label for="metadescription">Meta Description</label>
              <input type="text" class="form-control" name="metadescription" id="metadescription" aria-describedby="helpId" placeholder="">
              @error('metadescription')
                <small id="helpId" class="form-text text-danger">{{$message}}</small>                  
              @enderror
            </div>

            <div class="form-group">
              <label for="metakeywords">Meta Keywords</label>
              <input type="text" class="form-control" name="metakeywords" id="metakeywords" aria-describedby="helpId" placeholder="">
              @error('metakeywords')
                <small id="helpId" class="form-text text-danger">{{$message}}</small>                  
              @enderror
            </div>

            <div class="row">
              @include('admin.layouts.savebuttons')
              <div class="col md-3">
                  <a href="/admin/categories" class="btn btn-warning pull-right">Cancel</a>
              </div>
            </div>
        </form>
    </div>
    {{-- @include('admin.layouts.errors') --}}
@endsection