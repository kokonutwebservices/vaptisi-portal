@extends('admin.layouts.master')

@section('content')

<h1 class="mt-5">Video Καταχώρησης {{$entry->name}}</h1>

<div id="videoSortable" class="d-flex flex-row flex-wrap">

    @foreach ($videos as $video)

        @if ($video->YouTubeID)

            <div class="d-inline-flex img-thumbnail video-item m-2" id="{{$entry->id}}-{{$video->id}}">
                <div class="d-flex flex-column">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{$video->YouTubeID}}" allowfullscreen></iframe>
                    <div>
                        <i class="fas fa-arrows-alt text-primary" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Μετακίνηση"></i> 
                        <a href="/admin/entries/{{$entry->id}}/video/{{$video->id}}/delete" class="text-danger" data-toggle="tooltip" data-placement="top" title="Διαγραφή"><i class="fa fa-trash" aria-hidden="true"></i></a>
                    </div>                        
                </div>                   
            </div>   
            
        @elseif($video->VimeoID)

            <div class="d-inline-flex img-thumbnail video-item m-2" id="{{$entry->id}}-{{$video->id}}">
                <div class="d-flex flex-column">
                        <iframe src="https://player.vimeo.com/video/{{$video->VimeoID}}?byline=0&portrait=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
                <div>
                    <i class="fas fa-arrows-alt text-primary" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Μετακίνηση"></i> 
                    <a href="/admin/entries/{{$entry->id}}/video/{{$video->id}}/delete" class="text-danger" data-toggle="tooltip" data-placement="top" title="Διαγραφή"><i class="fa fa-trash" aria-hidden="true"></i></a>
                </div> 
            </div>

        @elseif($video->FacebookID)
            <div class="row">
                    <div class="d-inline-flex img-thumbnail video-item m-2" id="{{$entry->id}}-{{$video->id}}">
                        <div class="d-flex flex-column">
                            <!-- Load Facebook SDK for JavaScript -->
                            <div id="fb-root"></div>
                            <script>(function(d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s); js.id = id;
                                js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
                                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
        
                            <!-- Your embedded video player code -->
                            <div class="fb-video" data-href="https://www.facebook.com/facebook/videos/{{$video->FacebookID}}/" data-width="300" data-show-text="false">
                                <div class="fb-xfbml-parse-ignore">
                                    <a href="https://www.facebook.com/facebook/videos/{{$video->FacebookID}}/"></a>
                                </div>
                            </div>
                            <div>
                            <i class="fas fa-arrows-alt text-primary" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Μετακίνηση"></i> 
                            <a href="/admin/entries/{{$entry->id}}/video/{{$video->id}}/delete" class="text-danger" data-toggle="tooltip" data-placement="top" title="Διαγραφή"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
            </div>
        @endif
        
    @endforeach

</div>

<hr>
<h3 class="mb-3">Προσθήκη video</h3>

@include('admin.layouts.errors')

<form action="/admin/entries/{{$entry->id}}/video" method="POST">
    @csrf

    <div class="form-check form-check-inline">
        <label class="form-check-label mr-5">
            <input class="form-check-input" type="radio" name="vservice" id="" value="YouTube"> YouTube
        </label>
        <label class="form-check-label mr-5">
            <input class="form-check-input" type="radio" name="vservice" id="" value="Vimeo"> Vimeo
        </label>
        <label class="form-check-label mr-5">
            <input class="form-check-input" type="radio" name="vservice" id="" value="Facebook"> Facebook
        </label>
    </div>

    <div class="form-group mt-3">
      <label for="vlink">Video Link</label>
      <input type="text" class="form-control" name="vlink" id="" aria-describedby="helpId" placeholder="">
    </div>

    <button type="submit" class="btn btn-primary">Αποστολή</button>

</form>

<a href="/admin/entries/{{$entry->id}}/edit" class="btn btn-secondary mt-3">Επιστροφή στην καταχώρηση</a>

@include('admin.layouts.messages')
    
@endsection