@extends('admin.layouts.master')

@section('content')

<h1 class="mt-5">Καταχωρήσεις Vaptisi Portal</h1>

<a href="/admin/entries/create" class="btn btn-primary my-3">Προσθήκη Καταχώρησης</a>

<div class="float-right">
    <form action="/admin/entries/search" method="POST">
        @csrf
        
    <input type="text" name="query" id="query" placeholder="" value="{{$query}}">
        <button class="btn btn-primary" type="submit">Sumbit</button>
        <a class="btn btn-secondary" role="button" href="/admin/entries">Reset</a>
    </form>
</div>


<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Όνομα Καταχώρησης</th>
            <th scope="col">Κατηγορία</th>
            <th scope="col">Περιφέρειες</th>
            <th scope="col">Ημ. Λήξης</th>
            <th scope="col">Διαγραφή</th>
        </tr>
    </thead>
    @foreach ($entries as $entry)
        <tr>
            <th scope="col">{{$entry->id}}</th>
            <td><a href="/admin/entries/{{$entry->id}}/edit">{{$entry->name}}</a></td>
            <td>{{$entry->category->name}}</td>
            <td>
                <ul class="list-inline">
                @foreach ($entry->district as $district)
                    <li class="list-inline-item badge badge-secondary">{{$district->name}}</li>
                @endforeach
                </ul>
                
            </td>
            <td>{{$entry->end_at}}</td>
            <td>
                <form id="delete" action="/admin/entries/{{$entry->id}}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="badge badge-danger">Διαγραφή</button>
                </form>
            </td>
        </tr>
    @endforeach
    
</table>

<a href="/admin/entries/create" class="btn btn-primary mt-3">Προσθήκη Καταχώρησης</a>

@endsection