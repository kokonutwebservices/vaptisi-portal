@extends('admin.layouts.master')

@section('content')
    <div class="container">
        <h1 class="mt-5 mb-3">Μηνύματα Καταχώρησης: {{$entry->name}}</h1>
        <span class="float-right mb-2">Σύνολο: {{count($entry->messages)}}</span>
        <table class="table table-hover table-bordered">
            <thead class="thead-inverse">
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Καταχώρηση</th>
                    <th scope="col">Κατηγορία</th>
                    <th scope="col">Ημερομηνία</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($entry->messages as $message)
                    <tr>
                        <th scope="row">{{$message->id}}</th>
                        <td> <a href="/admin/messages/{{$message->id}}">{{$message->entry->name}}</a> </td>
                        <td> {{$message->entry->category->name}} </td>
                        <td> {{\Carbon\Carbon::parse($message->created_at)->format('d/m/Y, h:i:s')}} </td>
                        
                    </tr>
                    @endforeach
                    
                </tbody>
        </table>
        <span class="float-right mt3">
            <a class="btn btn-secondary" href="/admin/entries/{{$entry->id}}/edit"><i class="fas fa-undo-alt"></i> Επιστροφή</a>
        </span>
    </div>
@endsection