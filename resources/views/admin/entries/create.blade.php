@extends('admin.layouts.master')

@section('content')

<h1 class="my-5">Δημιουργία Καταχώρησης</h1>

<form action="/admin/entries" method="POST" enctype="multipart/form-data">
    @csrf 

    <div class="form-row">
        <div class="form-group col-md-6">
            Τύπος Καταχώρησης:
            <div class="form-check form-check-inline">
                <label class="form-check-label">
                    <input name="type" class="form-check-input" type="radio" id="basic" value="basic" checked> Basic
                </label>
            </div>
            <div class="form-check form-check-inline">
                <label class="form-check-label">
                    <input name="type" class="form-check-input" type="radio" id="premium" value="premium" > Premium
                </label>
            </div>
        </div>
    </div>
    
    <div class="form-row">
        <div class="col">
            <label for="client">Πελάτης</label>
            <select class="form-control" name="client_id" id="client" value="">
                <option value="" selected disabled>Επιλέξτε Πελάτη</option>
                @foreach ($clients as $client)
                    <option value="{{$client->id}}" {{old('client_id') == $client->id ? 'selected' : ''}}>{{$client->name}}</option>
                @endforeach        
            </select>
            @error('client_id')         
                <small id="helpId" class="form-text text-danger"> {{$message}} </small>            
            @enderror
        </div>        
    </div>

    <div class="row my-5">
        <div class="col-md-12">
            <h3>Περιφέρειες</h3>
            <div class="col-md-12">
                @foreach ($districts as $key => $district)
                    <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input 
                            class="form-check-input mx-2" 
                            type="checkbox" 
                            name="district[]" 
                            id="" 
                            value="{{$district->id}}"
                            {{ old('district.'.$key) == $district->id ? 'checked' : '' }}
                            
                            
                        > {{$district->name}}
                    </label>
                    </div>
                @endforeach               
            </div>
        </div>        
    </div>

    <div class="row my-5">
        <h3>Επαγγελματική Κατηγορία </h3>
        <div class="col-md-12">
            <div class="form-group">
              <label for="category_id">Κατηγορία</label>
              <select class="form-control" name="category_id" id="">
                
                <option value="" disabled selected>Επιλέξτε Κατηγορία</option>
                    @foreach ($categories as $category)
                        <option value="{{$category->id}}" {{old('category_id') == $category->id ? 'selected' : '' }} >{{$category->name}}</option>    
                    @endforeach    
              </select>
            </div>   
            @error('category_id')
                <small id="helpId" class="form-text text-danger"> {{$message}} </small> 
            @enderror         
        </div>        
    </div>
        
    
    <div class="form-group mt-3">
        <label for="name">Όνομα Επιχείρησης</label>
        <input type="text" class="form-control" name="name" id="name" placeholder="Όνομα επιχείρησης" value="{{ old('name') }}" >
        @error('name')
            <small id="helpId" class="form-text text-danger"> {{$message}} </small> 
        @enderror 
    </div>
    <div class="form-row mt-3">
        <div class="col mr-5">
            <label for="slug">Friendly URL</label>
            <input type="text" class="form-control" name="slug" id="slug" placeholder="slug - friendly url" value="{{ old('slug') }}" >
            @error('slug')
                <small id="helpId" class="form-text text-danger"> {{$message}} </small> 
            @enderror 
        </div>
        {{-- <div class="col">
            <div class="form-group ml-5">
              <label for="logo"> Λογότυπο
                  <div class="clearfix"></div>
                  <span class="btn btn-secondary mt-2" >Επιλογή Αρχείου</span></label>
              <input type="file" class="form-control-file" name="logo" id="logo" placeholder="" >
            </div>
        </div> --}}
        <div class="col">
            <div class="form-group ml-5">
                <label for="image"> Κεντρική Εικόνα
                    <div class="clearfix"></div>
                    <span class="btn btn-secondary mt-2" >Επιλογή Αρχείου</span></label>
                <input type="file" class="form-control-file" name="image" id="image" >
                @error('image')
                <small id="helpId" class="form-text text-danger"> {{$message}} </small> 
                @enderror
            </div>
        </div>
    </div>
    
    <div class="form-row mt-3 card-deck">
        @for ($i = 1; $i <= 2; $i++)
            <div class="col-md-6 card">
                <h4 class="card-header">Κατάστημα {{$i}}</h4>
                <div class="form-group">                
                    <label for="address[]">Διεύθυνση</label>
                    <input type="text" class="form-control" name="address[]" id="address[]" placeholder="Διεύθυνση Κατ/τος {{$i}}" value="{{ old('address.'.($i-1)) }}">
                </div>
                <div class="form-group">
                    <label for="tel[]">Τηλέφωνο</label>
                    <input type="text" class="form-control" name="tel[]" id="tel[]" placeholder="Τηλέφωνο Κατ/τος {{$i}}" value="{{ old('tel.'.($i-1)) }}">
                </div>
                <div class="form-group">
                    <label for="mob[]">Τηλέφωνο</label>
                    <input type="text" class="form-control" name="mob[]" id="mob[]" placeholder="Κινητό Κατ/τος {{$i}}" value="{{ old('mob.'.($i-1)) }}">
                </div>
                <div class="form-group">
                    <label for="email[]">Email</label>
                    <input type="text" class="form-control" name="email[]" id="email[]" placeholder="Email Κατ/τος {{$i}}" value="{{ old('email.'.($i-1)) }}">
                </div>
                <div class="form-group">
                    <label for="city[]">Πόλη</label>
                    <input type="text" class="form-control" name="city[]" id="city[]" placeholder="Πόλη/Περιοχή Κατ/τος {{$i}}" value="{{ old('city'.($i-1)) }}">
                </div>            
            </div>
        @endfor
    </div>

    <div class="form-row mt-4">
        <div class="col-md-6 mt-3">
            <label for="website">Website</label>
            <input type="text" class="form-control" name="website" id="website" placeholder="Website" value="{{ old('website') }}">
        </div>
        <div class="col-md-6 mt-3">
            <label for="facebook">Facebook</label>
            <input type="text" class="form-control" name="facebook" id="facebook" placeholder="Facebook Page" value="{{ old('facebook') }}">
        </div>
        <div class="col-md-6 mt-3">
            <label for="instagram">Instagram</label>
            <input type="text" class="form-control" name="instagram" id="instagram" placeholder="Instagram" value="{{ old('instagram') }}">
        </div>
    </div>

    <div class="form-group mt-4">
      <label for="profile">Κείμενο Προφίλ</label>
      <textarea name="profile" id="profile" class="form-control tinyMCE">{{ old('profile') }}</textarea>
    </div>

    <h4 class="mt-5">Meta Tags</h4>
    <div class="form-group">
        <label for="metatitle">Meta Title</label>
        <input type="text" name="metatitle" class="form-control" id="metatitle" placeholder="Meta Title" value="{{ old('metatitle') }}" >
        @error('metatitle')
        <small id="helpId" class="form-text text-danger"> {{$message}} </small> 
        @enderror
    </div>
    <div class="form-group">
        <label for="metadescription">Meta Description</label>
        <input type="text" name="metadescription" class="form-control" id="metadescription" placeholder="Meta Description" value="{{ old('metadescription') }}" >
        @error('metadescription')
            <small id="helpId" class="form-text text-danger"> {{$message}} </small> 
        @enderror
    </div>
    <div class="form-group">
        <label for="metakeywords">Meta Keywords</label>
        <input type="text" name="metakeywords" class="form-control" id="metakeywords" placeholder="Meta Keywords" value="{{ old('metakeywords') }}">
    </div>
    

   <div class="form-row mt-4">
        <div class="form-group col-md-6">
            Συμμετοχή New Life:
            <div class="form-check form-check-inline">
                <label class="form-check-label">
                    <input name="newlife" class="form-check-input" type="radio" id="inlineCheckbox1" value="1"> Ναι
                </label>
            </div>
            <div class="form-check form-check-inline">
                <label class="form-check-label">
                    <input name="newlife" class="form-check-input" type="radio" id="inlineCheckbox2" value="0" checked> Όχι
                </label>
            </div>
        </div>
        
        <div class="form-group col-md-6">
            Ενεργό:
            <div class="form-check form-check-inline">
                <label class="form-check-label">
                    <input name="active" class="form-check-input" type="radio" id="inlineCheckbox1" value="1" > Ναι
                </label>
            </div>
            <div class="form-check form-check-inline">
                <label class="form-check-label">
                    <input name="active" class="form-check-input" type="radio" id="inlineCheckbox2" value="0" checked> Όχι
                </label>
            </div>
        </div>
   </div>

   <h4 class="mt-4">Ημερομηνίες</h4>
   <p>Δημιουργία Καταχώρησης: {{ Carbon\Carbon::now()->toDateTimeString() }}</p>

   <div class="form-row">
       <div class="col-md-4">
            <label for="start">Έναρξη Καταχώρησης</label>
            <input type="date" name="start_at" id="start" class="form-control" value="{{ Carbon\Carbon::now()->toDateTimeString()}}">
       </div>
       <div class="col-md-4">
            <label for="end">Λήξη Καταχώρησης</label>
            <input type="date" name="end_at" id="end" class="form-control">
       </div>
       <div class="col-md-3">
            <p class="text-center"><strong>Απενεργοποίηση</strong></p>
            <p class="text-center">{{Carbon\Carbon::now()->addYear()->addMonth()->toDateTimeString()}}</p>
       </div>
   </div>

   <div class="row mt-4">
        @include('admin.layouts.savebuttons')
        <div class="col-md-3">
            <a href="/admin/entries" class="btn btn-warning">Cancel</a>
        </div>
   </div>
   

</form>
</div>
    
@include('admin.layouts.errors')

@endsection