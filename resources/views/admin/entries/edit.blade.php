@extends('admin.layouts.master')

@section('content')
    <h1 class="my-5">Επεξεργασία Καταχώρησης {{$entry->name}}</h1>

    <form action="/admin/entries/{{$entry->id}}" method="POST" enctype="multipart/form-data">
        @method('PATCH')
        @csrf 
    
        <div class="form-row">
            <div class="form-group col-md-6">
                Τύπος Καταχώρησης:
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input name="type" class="form-check-input" type="radio" id="basic" value="basic" {{$entry->type === 'basic' ? 'checked': ' ' }} > Basic
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input name="type" class="form-check-input" type="radio" id="premium" value="premium" {{$entry->type === 'premium' ? 'checked': ' ' }}> Premium
                    </label>
                </div>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="client">Πελάτης</label>
                <select class="form-control" name="client_id" id="client" required>
                    @foreach ($clients as $client)
                        @if ($client->id == $entry->client_id)
                        <option value="{{$client->id}}" selected>{{$client->name}}</option>
                        @else 
                        <option value="{{$client->id}}">{{$client->name}}</option>
                        @endif                        
                    @endforeach        
                </select>
            </div>
        </div>      

        <div class="row my-5">
            <div class="col-md-12">
                <h3>Περιφέρειες</h3>
                <div class="col-md-12">
                    @foreach ($districts as $district)
                        <div class="form-check form-check-inline">
                        <label class="form-check-label">
                            @if ($entry->district->contains($district->id))
                                <input class="form-check-input" type="checkbox" name="district[]" id="" value="{{$district->id}}" checked> {{$district->name}}
                            @else
                                <input class="form-check-input" type="checkbox" name="district[]" id="" value="{{$district->id}}"> {{$district->name}}
                            @endif                            
                        </label>  
                        </div>                          
                    @endforeach                
                    
                </div>        
            </div>
        </div>

        <div class="row my-5">
            <h3>Επαγγελματικές Κατηγορίες</h3>
            <div class="col-md-12">
                
                <div class="form-group">
                  <label for="category_id">Επαγγελματική Κατηγορία</label>
                  <select class="form-control" name="category_id" id="category">
                    
                    @foreach ($categories as $category)
                        <option value="{{$category->id}}" {{$category->id == $entry->category_id ? 'selected' : ''}} >{{$category->name}}</option>    
                    @endforeach                    
                    
                  </select>
                </div>

            </div>        
        </div>
        
        
        <div class="form-group mt-3">
            <label for="name">Όνομα Επιχείρησης</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Όνομα επιχείρησης" value="{{ $entry->name}}" required>
        </div>
        <div class="form-row mt-3">
            <div class="col mr-5">
                <label for="slug">Friendly URL</label>
                <input type="text" class="form-control" name="slug" id="slug" placeholder="slug - friendly url" value="{{ $entry->slug }}" required>
            </div>
            {{-- <div class="col">
                <div class="form-group ml-5">
                  <label for="logo"> Λογότυπο
                      <div class="clearfix"></div>
                      <img src="/storage/logos/{{$entry->logo}}" alt="" style="width: 200px;">
                      <span class="btn btn-secondary mt-2" >Επιλογή Αρχείου</span></label>
                  <input type="file" class="form-control-file" name="logo" id="logo" placeholder="" hidden>
                </div>
            </div> --}}
            <div class="col">
                <div class="form-group ml-5">
                    <label for="image"> Κεντρική Εικόνα
                        <div class="clearfix"></div>
                        <img src="/storage/entries/{{$entry->image}}" alt="" style="width:200px;">
                        <span class="btn btn-secondary mt-2" >Επιλογή Αρχείου</span></label>
                    <input type="file" class="form-control-file" name="image" id="image" placeholder="" hidden>
                </div>
            </div>
        </div>
    
        
        <div class="form-row mt-3 card-deck">
            
            @for ($i = 1; $i <= 2; $i++)

                <div class="col-md-6 card" data-shop="{{$i <= count($shops) ? $shops[$i-1]->id : ''}}">
                    <h4 class="card-header">Κατάστημα {{$i}}</h4>
                    <div class="form-group">                
                        <label for="address[]">Διεύθυνση</label>
                        <input type="text" class="form-control" name="address[]" id="address[]" placeholder="Διεύθυνση Κατ/τος {{$i}}" value="{{ $i > count($shops) ? '' : $shops[$i-1]->address }}">
                    </div>
                    <div class="form-group">
                        <label for="tel[]">Τηλέφωνο</label>
                        <input type="text" class="form-control" name="tel[]" id="tel[]" placeholder="Τηλέφωνο Κατ/τος {{$i}}" value="{{ $i > count($shops) ? '' : $shops[$i-1]->tel }}">
                    </div>
                    <div class="form-group">
                        <label for="mob[]">Τηλέφωνο</label>
                        <input type="text" class="form-control" name="mob[]" id="mob[]" placeholder="Κινητό Κατ/τος {{$i}}" value="{{ $i > count($shops) ? '' : $shops[$i-1]->mob }}">
                    </div>
                    <div class="form-group">
                        <label for="email[]">Email</label>
                        <input type="text" class="form-control" name="email[]" id="email[]" placeholder="Email Κατ/τος {{$i}}" value="{{ $i > count($shops) ? '' : $shops[$i-1]->email }}">
                    </div>
                    <div class="form-group">
                        <label for="city[]">Πόλη</label>
                        <input type="text" class="form-control" name="city[]" id="city[]" placeholder="Πόλη/Περιοχή Κατ/τος {{$i}}" value="{{ $i > count($shops) ? '' : $shops[$i-1]->city }}">
                    </div>
                    @if ($i <= count($shops))
                        <div class="form-group">
                            <button type="button" name="" id="" data-shop="{{$shops[$i-1]->id}}" class="btn btn-danger float-right deleteShop">X</button>
                        </div>   
                    @endif
                </div>
                
            @endfor
        </div>
    
        <div class="form-row mt-4">
            <div class="col-md-6 mt-3">
                <label for="website">Website</label>
                <input type="text" class="form-control" name="website" id="website" placeholder="Website" value="{{ $entry->website }}">
            </div>
            <div class="col-md-6 mt-3">
                <label for="facebook">Facebook</label>
                <input type="text" class="form-control" name="facebook" id="facebook" placeholder="Facebook Page" value="{{ $entry->facebook }}">
            </div>
            <div class="col-md-6 mt-3">
                <label for="instagram">Instagram</label>
                <input type="text" class="form-control" name="instagram" id="instagram" placeholder="Instagram" value="{{ $entry->instagram }}">
            </div>
        </div>
    
        <div class="form-group mt-4">
          <label for="profile">Κείμενο Προφίλ</label>
          <textarea name="profile" id="profile" class="form-control tinyMCE">{{ $entry->profile }}</textarea>
        </div>
    
        <h4 class="mt-5">Meta Tags</h4>
        <div class="form-group">
            <label for="metatitle">Meta Title</label>
            <input type="text" name="metatitle" class="form-control" id="metatitle" placeholder="Meta Title" value="{{ $entry->metatitle }}" required>
        </div>
        <div class="form-group">
            <label for="metadescription">Meta Description</label>
            <input type="text" name="metadescription" class="form-control" id="metadescription" placeholder="Meta Description" value="{{ $entry->metadescription }}" required>
        </div>
        <div class="form-group">
            <label for="metakeywords">Meta Keywords</label>
            <input type="text" name="metakeywords" class="form-control" id="metakeywords" placeholder="Meta Keywords" value="{{ $entry->metakeywords }}">
        </div>
        
    
       <div class="form-row mt-4">
            <div class="form-group col-md-6">
                Συμμετοχή New Life:
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input name="newlife" class="form-check-input" type="radio" id="inlineCheckbox1" value="1" 
                        @if ($entry->newlife == 1)
                            checked
                        @endif
                        > Ναι
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input name="newlife" class="form-check-input" type="radio" id="inlineCheckbox2" value="0" 
                        @if ($entry->newlife == 0)
                            checked
                        @endif 
                        > Όχι
                    </label>
                </div>
            </div>
            
            <div class="form-group col-md-6">
                Ενεργό:
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input name="active" class="form-check-input" type="radio" id="inlineCheckbox1" value="1" 
                        @if ($entry->active == 1)
                            checked
                        @endif 
                        > Ναι
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input name="active" class="form-check-input" type="radio" id="inlineCheckbox2" value="0" 
                        @if ($entry->active == 0)
                            checked
                        @endif
                        > Όχι
                    </label>
                </div>
            </div>
       </div>
    
       
       <livewire:admin.renew-entry :entry="$entry"/>

       <col-md-3>
            <a href="/admin/entries/{{$entry->id}}/albums" class="btn btn-secondary my-4 mr-2">Φωτογραφίες</a>
       </col-md-3>
       <col-md-3>
            <a href="/admin/entries/{{$entry->id}}/video" class="btn btn-secondary my-4 ml-2">Video</a>
       </col-md-3>
       <col-md-3>
            <a href="/admin/entries/{{$entry->id}}/messages" class="btn btn-secondary my-4 ml-2">Μηνύματα <span class="ml-2">({{count($entry->messages)}})</span></a>
       </col-md-3>
       <col-md-3>
            <a href="/{{$entry->category->slug}}/{{$entry->slug}}" target="_blank" class="btn btn-primary my-4 ml-2">Προβολή καταχώρησης <span class="ml-2">({{count($entry->messages)}})</span></a>
       </col-md-3>
    
       <div class="row mt-4">
            @include('admin.layouts.savebuttons')
            <div class="col-md-3">
                <a href="/admin/entries" class="btn btn-warning">Cancel</a>
            </div> 
    </form>

    <div class="col md-3">
        <form id="delete" method="POST" action="/admin/entries/{{$entry->id}}" >
            @method('DELETE')
            @csrf
            <button type="submit" class="btn btn-danger">Διαγραφή</button>
        </form>
    </div>
</div>
    @include('admin.layouts.errors')


@endsection