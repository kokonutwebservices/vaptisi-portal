@extends('admin.layouts.master')

@section('content')

<h1 class="mt-5">Καταχωρήσεις Vaptisi Portal</h1>
<span> {{count($entries)}} Καταχωρήσεις</span>
<div class="clearfix"></div>


<livewire:admin.entries-list />

<a href="/admin/entries/create" class="btn btn-primary mt-3">Προσθήκη Καταχώρησης</a>

@endsection