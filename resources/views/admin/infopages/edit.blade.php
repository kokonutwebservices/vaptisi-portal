@extends('admin.layouts.master')

@section('content')

<h1 class="my-5">Επεξεργασία Σελίδας: {{ $infopage->title }}</h1>

<form action="/admin/infopages/{{$infopage->id}}" method="POST" enctype="multipart/form-data">

    @csrf
    @method('PATCH')

    <div class="form-group">
        <label for="title">Τίτλος Σελίδας</label>
        <input type="text"
          class="form-control" name="title" id="title" aria-describedby="helpId" value="{{$infopage->title}}" required>
          @error('title')
              <small id="helpId" class="form-text text-danger">{{$message}}</small>
          @enderror
      </div>
  
      <div class="form-group">
          <label for="slug">Slug</label>
          <input type="text"
            class="form-control" name="slug" id="slug" aria-describedby="helpId" value="{{$infopage->slug}}" required>
          @error('slug')
              <small id="helpId" class="form-text text-danger">{{$message}}</small>
          @enderror
      </div>
  
      <div class="form-group">
        <label for="metatitle">Meta Title</label>
        <input type="text"
          class="form-control" name="metatitle" id="metatitle" aria-describedby="helpId" value="{{$infopage->metatitle}}" required>
          @error('metatitle')
              <small id="helpId" class="form-text text-danger">{{$message}}</small>
          @enderror
      </div>
  
      <div class="form-group">
        <label for="metadescription">Meta Description</label>
        <input type="text"
          class="form-control" name="metadescription" id="metadescription" aria-describedby="helpId" value="{{$infopage->metadescription}}">
      </div>
  
      <div class="form-group">
        <label for="body">Κείμενο</label>
        <textarea class="form-control tinyMCE" name="body" id="body" rows="3">{{$infopage->body}}</textarea>
      </div>
  
      <div class="form-group">
          <label for="image">Εικόνα</label>
          <input type="file" class="form-control-file" name="image" id="image" value="{{ $infopage->image ? $infopage->image : '' }}" aria-describedby="fileHelpId">
          @if ($infopage->image)
              <img src="/storage/infopages/{{$infopage->image}}" alt="" style="width: 300px">
          @endif
         
          <div class="form-check">
            <label class="form-check-label">
              <input type="checkbox" class="form-check-input" name="delete" id="" value="checked">
              Επιλέξτε για διαγραφή
            </label>
          </div>
      </div>
  
      <div class="form-group">
        <label for="alt">Alt tag</label>
        <input type="text"
          class="form-control" name="alt" id="alt" aria-describedby="helpId" value="{{$infopage->alt}}">
      </div>
  
      <div class="row mt-4">
          @include('admin.layouts.savebuttons')
          <div class="col-md-3">
              <a href="/admin/infopages" class="btn btn-warning">Cancel</a>
          </div>
      

</form>

<form action="/admin/infopages/{{$infopage->id}}" method="POST">
    @csrf
    @method('DELETE')

    <button type="submit" class="btn btn-danger">DELETE</button>

</form>

</div>
    
@endsection