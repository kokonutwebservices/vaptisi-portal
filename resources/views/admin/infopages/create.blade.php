@extends('admin.layouts.master')

@section('content')

<h1 class="mt-5">Δημιουργία Σελίδας Κατηγοριών</h1>

<form action="/admin/infopages" method="post" enctype="multipart/form-data">
    @csrf

    <div class="form-group">
      <label for="title">Τίτλος Σελίδας</label>
      <input type="text"
        class="form-control" name="title" id="title" aria-describedby="helpId" placeholder="" required>
        @error('title')
            <small id="helpId" class="form-text text-danger">{{$message}}</small>
        @enderror
    </div>

    <div class="form-group">
        <label for="slug">Slug</label>
        <input type="text"
          class="form-control" name="slug" id="slug" aria-describedby="helpId" placeholder="" required>
        @error('slug')
            <small id="helpId" class="form-text text-danger">{{$message}}</small>
        @enderror
    </div>

    <div class="form-group">
      <label for="metatitle">Meta Title</label>
      <input type="text"
        class="form-control" name="metatitle" id="metatitle" aria-describedby="helpId" placeholder="" required>
        @error('metatitle')
            <small id="helpId" class="form-text text-danger">{{$message}}</small>
        @enderror
    </div>

    <div class="form-group">
      <label for="metadescription">Meta Description</label>
      <input type="text"
        class="form-control" name="metadescription" id="metadescription" aria-describedby="helpId" placeholder="">
    </div>

    <div class="form-group">
      <label for="body">Κείμενο</label>
      <textarea class="form-control tinyMCE" name="body" id="body" rows="3"></textarea>
    </div>

    <div class="form-group">
        <label for="image">Εικόνα</label>
        <input type="file" class="form-control-file" name="image" id="image" placeholder="" aria-describedby="fileHelpId">
    </div>

    <div class="form-group">
      <label for="alt">Alt tag</label>
      <input type="text"
        class="form-control" name="alt" id="alt" aria-describedby="helpId" placeholder="">
    </div>

    <div class="row mt-4">
        @include('admin.layouts.savebuttons')
        <div class="col-md-3">
            <a href="/admin/infopages" class="btn btn-warning">Cancel</a>
        </div>
    </div>

</form>
    
@endsection