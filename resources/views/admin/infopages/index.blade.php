@extends('admin.layouts.master')

@section('content')

<h1 class="my-5">Σελίδες Πληροφοριών Vaptisi Portal</h1>

<div id="infoPagesSortable">
@foreach ($infoPages as $page)

    <div id="{{$page->id}}" class="py-1">
        <a class="text-info" href="/admin/infopages/{{$page->id}}/edit">{{$page->title}}</a>
    </div>
    
@endforeach
</div>

<a class="btn btn-primary mt-5" href="/admin/infopages/create">Δημιουργία Σελίδας</a>
    
@endsection