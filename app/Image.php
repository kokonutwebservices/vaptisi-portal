<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Intervention\Image\Facades\Image as InterImage;

class Image extends Model
{
    protected $guarded = [];

    public function album(){
        return $this->belongsTo(Album::class);
    }

    public function imageOrder(){
        return $this->max('order') +1;
    }

    public function tags(){
        return $this->belongsToMany(Tag::class);
    }

    public function entry(){
        return $this->belongsTo(Entry::class);
    }


    public function imageSize() {

        // $imagePath = 'storage/uploads/'.$this->album->entry->id.'/'.$this->album->id.'/'.$this->image;
        // dd(public_path('storage/uploads/'.$this->album->entry->id.'/'.$this->album->id.'/'.$this->image));
        $imagePath = public_path('/storage/uploads/'.$this->album->entry->id.'/'.$this->album->id.'/'.$this->image);

        $imageWidth = InterImage::make($imagePath)->width();
        $imageHeight = InterImage::make($imagePath)->height();
        echo $imageWidth.'x'.$imageHeight;
    }
}
