<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $guarded = [];

    public function entries() {
        return $this->belongsToMany(Entry::class);
    }

    public function categoryEntries (array $entryIds) {

        return $this->belongsToMany(Entry::class)->wherePivotIn('entry_id', $entryIds);

    }
}
