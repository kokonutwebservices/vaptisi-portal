<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $guarded = [];

    public function entry(){
        return $this->belongsTo(Entry::class);
    }

    public function images(){
        return $this->hasMany(Image::class);
    }

    public function albumsOrder(){
        // return $this->max('order')+1;
        $albums = $this->all();
        foreach ($albums as $album){
            $album->order = $album->order+1;    
            $album->save();        
        }
        return $this->order = 1;
    }
}
