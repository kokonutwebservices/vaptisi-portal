<?php

namespace App\Http\Livewire\Site;

use App\Mail\EntryPhoneNotification;
use App\Shop;
use Livewire\Component;
use Illuminate\Support\Facades\Mail;

class ContactInfo extends Component
{

    public $entry;


    public function phoneNotification(Shop $shop)
    {
        
        Mail::send(new EntryPhoneNotification($shop));

    }

    public function render()
    {
        return view('livewire.site.contact-info');
    }
}
