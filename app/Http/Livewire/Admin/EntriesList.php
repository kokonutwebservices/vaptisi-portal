<?php

namespace App\Http\Livewire\Admin;

use App\Entry;
use Livewire\Component;

class EntriesList extends Component
{
    public $searchTerm;

    public function clearSearch()
    {
        $this->reset('searchTerm');
    }

    public function render()
    {
        $search = '%'.$this->searchTerm.'%';

        $entries = Entry::where('name', 'like', $search)->get();

        return view('livewire.admin.entries-list', compact('entries'));
    }
}
