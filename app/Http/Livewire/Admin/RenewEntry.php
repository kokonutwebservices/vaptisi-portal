<?php

namespace App\Http\Livewire\Admin;

use Carbon\Carbon;
use Livewire\Component;

class RenewEntry extends Component
{
    public $entry;
    public $start;
    public $end;

    public function renew() {
       
        $this->start = Carbon::createFromFormat("d/m/Y",$this->start)->addYear()->format("d/m/Y");       
        $this->end = Carbon::createFromFormat("d/m/Y",$this->end)->addYear()->format("d/m/Y");

    }

    public function mount()
    {
        $this->start = Carbon::parse($this->entry->start_at)->format("d/m/Y");
        $this->end = Carbon::parse($this->entry->end_at)->format("d/m/Y");
    }

  
    

    public function render()
    {
        return view('livewire.admin.renew-entry');
    }
}
