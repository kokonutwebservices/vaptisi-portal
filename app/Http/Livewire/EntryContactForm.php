<?php

namespace App\Http\Livewire;

use App\Category;
use App\ClientMessage;
use App\Entry;
use App\Mail\EntryContactForm as MailEntryContactForm;
use App\Mail\EntryContactFormCopy;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;

class EntryContactForm extends Component
{

    public $entry;
    public $name;
    public $email;
    public $phone;
    public $message;
    public $terms;
    public $successMessage = NULL;

    protected $rules = [
        'name' => 'required',
        'email' => 'required|email',
        'phone' => 'required|numeric|min:10',
        'message' => 'required|min:20',
        'terms' => 'required:1'
    ];

    public function updated($property) {
        $this->validateOnly($property);
    }

    public function submitForm () {
        
        $data = $this->validate();
        

        $data['entry_id'] = $this->entry->id;
        $data['entry_name'] = $this->entry->name;
        $data['category_id'] = $this->entry->category->id;

        unset($data['terms']);

        ClientMessage::create($data);

        $message = ClientMessage::latest()->first();

        $entry = Entry::find($message->entry_id);

        $category = Category::find($entry->category->id);

        Mail::send(new MailEntryContactForm($message, $entry, $category));
        Mail::send(new EntryContactFormCopy($message, $category, $entry));

        $this->reset();

        $this->successMessage = 'Το μήνυμα στάλθηκε με επιτυχία!';

    }

    public function render()
    {
        return view('livewire.entry-contact-form');
    }
}
