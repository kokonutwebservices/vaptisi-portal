<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Entry;
use \App\Category;
use \App\District;
use App\Image;

class SiteHomePageController extends BaseController
{
    public function index(){

        // $entries = Entry::all()->take(12);

        $districts = District::all();

        $latest = Entry::orderByDesc('end_at')->take(8)->get();

        return view('site.index', compact('districts', 'latest'));

    }
}
