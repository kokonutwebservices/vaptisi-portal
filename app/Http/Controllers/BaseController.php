<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\InfoPage;
use Illuminate\Support\Facades\View;

class BaseController extends Controller
{
    
    public function __construct()
    {

        // Για να φορτώνεται σε όλα τα views η μεταβλητή categories που χρησιμοποιείται στο nav. Από εδώ: https://stackoverflow.com/questions/28608527/how-to-pass-data-to-all-views-in-laravel-5

        $parentCategories = Category::where('parent_id', 0)->orderBy('order')->get();
        $categories = Category::where('parent_id', '>', 0)->orderBy('order')->get();
        
        $vaptisiCategories = Category::where('parent_id', 1)->orderBy('order')->get();
        $dexiosiCategories = Category::where('parent_id', 2)->orderBy('order')->get();

        $infoPages = InfoPage::all()->sortBy('order');

        View::share(['vaptisiCategories'=> $vaptisiCategories, 'dexiosiCategories'=>$dexiosiCategories, 'parentCategories' => $parentCategories, 'categories'=>$categories, 'infoPages' => $infoPages]);

    }

}
