<?php

namespace App\Http\Controllers;

use App\Image;
use App\Album;
use App\Entry;
use Storage;
use File;
use InterImage;
use Illuminate\Http\Request;

class ImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Entry $entry, Album $album)
    {

        if(count($request->image) > $request->available ) {
            return redirect()->back()->withErrors(['Δεν μπορείτε να προσθέσετε περισσότερες από '. $request->available .' εικόνες']);
        } else {
            $this->validate(request(), [
                'image.*' => 'max:2000|mimes:jpeg,jpg'
            ]);
        }

        
        
        //Για το μαζικό upload το πρόβλημα με το filename είναι ότι με το timestamp τα ονόματα συνέπιπταν και δεν αποθηκεύονταν όλα τα αρχεία.
        //Παρατήρησα ότι στο tmp το αρχείο έπαιρνε ένα μοναδικό όνομα, το οποίο απομονώνω και το προσθέτω στο slug της καταχώρησης.
        // Σημείωση από Gamos Portal
        $order= $album->images->max('order');

        foreach ($request->image as $file) {

            $filePath = $file->getPathName();
            $file_sub = substr($filePath, -9, -4);
            $ext = $file->getClientOriginalExtension(); //Get the extension
            $filename = $entry->slug. "-" . $file_sub.".".$ext;
            $upload = $file->storeAs("uploads/$entry->id/$album->id", $filename); //Store file
            $order ++;

            if (!Storage::exists("uploads/$entry->id/$album->id/small")) {
                Storage::makeDirectory("uploads/$entry->id/$album->id/small");
            } 
            InterImage::make(public_path("storage/uploads/$entry->id/$album->id/$filename"))->resize(300, null, function($constraint){
                $constraint->aspectRatio();
            })->save(public_path("storage/uploads/$entry->id/$album->id/small/$filename"));
            
            $image = new Image;
            $image->album_id = $album->id;
            $image->image = $filename;
            $image->order = $order;
            $image->save();
        }

        return redirect()->back();
    }

    public function imageDescriptionStore(Request $request, Entry $entry, Album $albumm){
        
        $image = Image::find($request->image);

        $image->description = $request->description;

        $image->save();

        return redirect()->back();

    }

    

    /**
     * Display the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show(Image $image)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Entry $entry, Album $albumm, Image $image)
    {
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */

    public function destroy(Request $request, Entry $entry, Album $album, Image $image) {
        
        Storage::delete("uploads/$entry->id/$album->id/$image->image");
        $image->delete();

        return redirect()->back();
    }

    public function order(Entry $entry, Album $album)
    {
        $order = $_POST['order'];

        foreach ($order as $neworder => $item) {
            
            $imageId = explode("-", $item)[2];

            $image = Image::find($imageId);

            $image->order = $neworder + 1;

            $image->save();


        }
    }

    public function thumbs() {

        $entries = Entry::all();

        foreach ($entries as $entry) {

            foreach ($entry->albums as $album) {

                $imageFolder = "/uploads/$entry->id/$album->id";

                $images = Image::where('album_id', $album->id)->get();

                if (!Storage::exists($imageFolder.'/small')) {
                    Storage::makeDirectory("$imageFolder/small");
                } 


                foreach ($images as $image) {

                    InterImage::make('storage/'.$imageFolder.'/'.$image->image)->resize(300, null, function($constraint){
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->save('storage/'.$imageFolder.'/small/'.$image->image);
                    
                }
            }
        }
        // Σημ: το Storage και το Intervention Image "βλέπουν" διαφορετικό φάκελο ως αρχικό, γι' αυτό στο δεύτερο προσθέτω το "storage/"

    }
}
