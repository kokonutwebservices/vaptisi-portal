<?php

namespace App\Http\Controllers;

use App\Video;
use App\Entry;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Entry $entry)
    {
        $videos = Video::where('entry_id', $entry->id)->orderBy('order')->get();
        return view('admin.videos.index', compact('entry', 'videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Entry $entry)
    {
        request()->validate([
            'vservice' => 'required',
            'vlink' => 'required'
        ]);


        $video = new Video;

        $service = request('vservice');
        $url = request('vlink');

        if($service === 'YouTube') {

            if (preg_match('/youtube\.com\/watch\?v=([^\&\?\/]+)/', $url, $id)) {
                $values = $id[1];
              } else if (preg_match('/youtube\.com\/embed\/([^\&\?\/]+)/', $url, $id)) {
                $values = $id[1];
              } else if (preg_match('/youtube\.com\/v\/([^\&\?\/]+)/', $url, $id)) {
                $values = $id[1];
              } else if (preg_match('/youtu\.be\/([^\&\?\/]+)/', $url, $id)) {
                $values = $id[1];
              }
              else if (preg_match('/youtube\.com\/verify_age\?next_url=\/watch%3Fv%3D([^\&\?\/]+)/', $url, $id)) {
                  $values = $id[1];
              } else {
                  return redirect()->back()->withErrors('Δεν επιλέξατε video από το YouTube');
              }
              
              $video->YouTubeID = $id[1];

        } elseif ($service === 'Vimeo'){

            if(preg_match_all('#(http://vimeo.com)/([0-9]+)#i',$url,$id) OR preg_match_all('#(https://vimeo.com)/([0-9]+)#i',$url,$id)){
                $video->VimeoId = $id[2][0];
            } else {
                return redirect()->back()->withErrors('Δεν επιλέξατε Video από το Vimeo');
            }

        } elseif ($service === 'Facebook') {
            
            if(preg_match('~/videos/(?:t\.\d+/)?(\d+)~i',$url,$id)){
                $video->FacebookId = $id[1];                
            } else {
                return redirect()->back()->withErrors('Δεν επιλέξατε Video από το Facebook');
            }  
            
        } 

        $video->entry_id = $entry->id;
        $video->order = $video->newVideoOrder();

        $video->save();

        return redirect()->back()->withMessage('Το video προστέθηκε');
        

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entry $entry, Video $video)
    {
        $video = Video::find($video->id);

        $video->delete();

        return redirect()->back()->withMessage('Το video διαγράφηκε');
    }

    public function order(Entry $entry) {

        $order = $_POST['order'];

        foreach ($order as $neworder => $item) {

            $videoId = explode("-", $item)[1];

            $video = Video::find($videoId);

            $video->order = $neworder + 1;

            $video->save();           

        }

    }
}
