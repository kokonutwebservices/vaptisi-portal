<?php

namespace App\Http\Controllers;

use App\Album;
use App\Category;
use App\ClientMessage;
use App\Entry;
use App\Mail\EntryContactForm;
use App\Mail\EntryContactFormCopy;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SiteEntryController extends BaseController
{
    public function show($categorypage, $entryslug) {

        // $category = Page::where('slug', $categorypage)->firstOrFail();

        $entry = Entry::with('category')->where('slug', $entryslug)->firstOrFail();

        return view('site.entry.show', compact('entry'));

    }

    // public function album($categorypage, $entryslug, Album $album) {

    //     $category = Category::where('parent_id', '>', 0)->where('slug', $categorypage)->first();

    //     $entry = Entry::where('slug', $entryslug)->firstOrFail();

    //     foreach ($entry->albums as $album) {
    //         return $album->images;
    //     }
    // }

    public function contactForm(Request $request, Category $category, Entry $entry) {

        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric|min:10',
            'message' => 'required',
            'terms' => 'required:1'
        ]);

    
        // Εγγραφή στη βάση δεδομένων
        $message = new ClientMessage();
        $message->entry_id = $entry->id;
        $message->category_id = $category->id;
        $message->name = request('name');
        $message->email = request('email');
        $message->phone = request('phone');
        $message->message = request('message');

        $message->save();

        
        // Αποστολή email
        Mail::send(new EntryContactForm($request, $category, $entry));
        Mail::send(new EntryContactFormCopy($request, $category, $entry));


        // Redirect
        return redirect()->back()->with('message', 'Το μήνυμα στάλθηκε με επιτυχία');

    }
}
