<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Storage;

class AdminCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Category $category)
    {
        $categories = Category::all()->sortBy('order');
        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Category $category)
    {
        $parents = Category::where('parent_id', '0')->get();

        return view ('admin.categories.create', compact('parents'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $attributes = request()->validate([
            'name' => 'required',
            'slug' => 'required|max:60',
            'metatitle' => 'required|min:5,max:80'
        ]);

        $attributes['description'] = request('description');
        $attributes['parent_id'] = request('parent_id');
        $attributes['metatitle'] = request('metatitle');
        $attributes['metadescription'] = request('metadescription');
        $attributes['metakeywords'] = request('metakeywords');


        //Category Image
        if ($request->hasFile('image')){
            $file = $request->file('image');
            $ext = $file->getClientOriginalExtension();
            $fileNameBase = request('slug');
            $fileName = $fileNameBase.'.'.$ext;
            $file->storeAs('categories', $fileName);

            $attributes['image'] = $fileName;        
        }


        $maxOrder = Category::where('parent_id', $request->parent_id)->get()->max('order');

        $attributes['order'] = $maxOrder +1;


        Category::create($attributes);    
        
        $id = Category::all()->last()->id;

        if($request->submitButton == 'save'){
            return redirect("/admin/categories/$id/edit");
        } elseif ($request->submitButton == 'save-close') {
            return redirect("/admin/categories");
        } elseif ($request->submitButton == 'save-new'){
            return redirect("/admin/categories/create");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $parents = Category::where('parent_id', '0')->get();

        return view('admin.categories.edit', compact(['category', 'parents']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $attributes = request()->validate([
            'name' => 'required',
            'slug'=> 'required',
            'metatitle' => 'required'
        ]);
        
        // Category Image
        if($request->hasFile('image')){
            $file = $request->file('image');
            $ext = $file->getClientOriginalExtension();
            $fileNameBase = request('slug');
            $fileName = $fileNameBase.'.'.$ext;
            $file->storeAs('categories', $fileName);
        
        } else {
            $fileName = $category->image;            
        }

        
        $attributes['image'] = $fileName;

        $attributes['parent_id'] = request('parent_id');
        $attributes['description'] = request('description');
        $attributes['metatitle'] = request('metatitle');
        $attributes['metadescription'] = request('metadescription');
        $attributes['metakeywords'] = request('metakeywords');

        // Υπολογισμός νέας σειράς order
        //Αν το parent_id μένει ίδιο
        if(request('parent_id') == $category->parent_id){
            //Αν το νέο order είναι μεγαλύτερο
            if(request('order') > $category->order) {
                $orders = Category::where('order', '>', $category->order)->where('order', '<=', request('order'))->get();
                foreach ($orders as $order) {
                    $order->order = $order->order-1;
                    $order->save();
                }
                $attributes['order'] = request('order');
            } elseif (request('order') < $category->order) {
                
                // Αν το νέο order είναι μικρότερο
                $orders = Category::where('order', '<', $category->order)->where('order', '>=', request('order'))->get();
                foreach ($orders as $order) {
                    $order->order = $order->order+1;
                    $order->save();
                }
                $attributes['order'] = request('order');
            }
            else {
                // Αν το νέο order είναι ίδιο
                $attributes['order'] = $category->order;
            }
        } else {
            //Αν αλλάξει parent, πάει στο τέλος
            $attributes['order'] = 100;
        }
        
        $category->update($attributes);

        if($request->submitButton == 'save'){
            return redirect("/admin/categories/$category->id/edit");
        } elseif ($request->submitButton == 'save-close') {
            return redirect("/admin/categories");
        } elseif ($request->submitButton == 'save-new'){
            return redirect("/admin/categories/create");
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        
        Storage::delete("categories/$category->image");
     
        $category->delete();

        return redirect('/admin/categories');
    }

    public function order(){

        $orders = $_POST['order'];

        foreach ($orders as $key => $categoryId) {
            
            $parentCategory = Category::find($categoryId);

            $parentCategory->order = $key+1;

            $parentCategory->save();

        }
    }

    public function suborder(){
        $orders = $_POST['order'];

        foreach ($orders as $key=>$categoryId) {

            $category = Category::find($categoryId);

            $category->order = $key+1;

            $category->save();

        }
    }
}
