<?php

namespace App\Http\Controllers;

use App\Entry;
use App\Category;
use App\District;
use Illuminate\Http\Request;

class SitemapController extends Controller
{
    public function index()
    {
        $categories = Category::where('parent_id', '>', 0)->with('entries')->get();
        $districts = District::all();
        
        return view('site.sitemaps.index', compact('categories', 'districts'));
    }
}
