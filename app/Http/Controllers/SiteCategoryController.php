<?php

namespace App\Http\Controllers;

use App\Category;
use App\Page;
use App\Entry;
use App\District;
use App\InfoPage;
use Illuminate\Http\Request;

class SiteCategoryController extends BaseController
{
    
    public function index($slug)
    {

        $page = Page::where('slug', $slug)->first();
        $infopage = InfoPage::where('slug', $slug)->first();

        if($page) {
            $entries = Entry::where(['category_id' => $page->category_id,'active' => 1])->orderBy('order')->get();
        
            // Για φιλτράρισμα του πίνακα district_entry με βάση τα id των entries, τα οποία αποτελούν ένα array. Μέθοδος από εδώ: https://laracasts.com/discuss/channels/eloquent/filter-pivot-table-with-array-values
    
            $entryIds = $entries->pluck('id')->toArray();    
            
            $districts = District::whereHas('entries', function ($query) use ($entryIds){
                return $query->whereIn('entry_id', $entryIds);
            })->get();
            
            return view ('site.pages.show', compact('page', 'entries', 'districts'));

        } elseif($infopage) {
            return view ('site.infopages.show', compact('infopage'));
        }

    }

    public function district($categorypage, $district)
    {
        $page = Page::where('slug', $categorypage)->first();
        $category = $page->category;
        $district = District::where('slug', $district)->first();

        $districtId = $district->id;
        

        $entries = Entry::where(['category_id' => $category->id, 'active' => 1])->whereHas('district', function($query) use ($districtId){
            return $query->where('district_id', $districtId);
        })
        ->orderBy('order')
        ->get();

       
        return view('site.pages.filtered', compact('page', 'category', 'entries', 'district'));

    }

}
