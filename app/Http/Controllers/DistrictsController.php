<?php

namespace App\Http\Controllers;

use App\District;
use Illuminate\Http\Request;

class DistrictsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(District $district)
    {
        $districts = District::all();
        return view ('admin.districts.index', compact('districts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.districts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = request()->validate([
            'name' => 'required|min:3',
            'slug' => 'required|min:3'
        ]);

        $attributes['metatitle'] = request('metatitle');
        $attributes['metadescription'] = request('metadescription');
        $attributes['metakeywords'] = request('metakeywords');

        District::create($attributes);

        $id = District::all()->last()->id;

        if($request->submitButton == 'save'){
            return redirect("/admin/districts/$id/edit");
        } elseif ($request->submitButton == 'save-close') {
            return redirect("/admin/districts");
        } elseif ($request->submitButton == 'save-new'){
            return redirect("/admin/districts/create");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\District  $district
     * @return \Illuminate\Http\Response
     */
    public function show(District $district)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\District  $district
     * @return \Illuminate\Http\Response
     */
    public function edit(District $district)
    {
        return view('admin.districts.edit', compact('district'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\District  $district
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, District $district)
    {
        $attributes = request()->validate([
            'name' => 'required|min:3',
            'name' => 'required|min:3'
        ]);

        $attributes['metatitle'] = request('metatitle');
        $attributes['metadescription'] = request('metadescription');
        $attributes['metakeywords'] = request('metakeywords');

        $district->update($attributes);

        if($request->submitButton == 'save'){
            return redirect("/admin/districts/$district->id/edit");
        } elseif ($request->submitButton == 'save-close') {
            return redirect("/admin/districts");
        } elseif ($request->submitButton == 'save-new'){
            return redirect("/admin/districts/create");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\District  $district
     * @return \Illuminate\Http\Response
     */
    public function destroy(District $district)
    {
        $district->delete();

        return redirect('admin/districts');
    }
}
