<?php

namespace App\Http\Controllers;

use App\Page;
use App\Category;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::all()->sortBy('order');

        return view('admin.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('parent_id', '>' , 0)->get();

        return view('admin.pages.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'category_id' => 'required',
            'title' => 'required|min:5',
            'slug' => 'required|min:5',
            'description' => 'required|min:20',
            'subtitle' => 'required|min:min:5',
            'metatitle' => 'required|min:5|max:80',
            'metadescription' => 'required|min:5|max:180'
        ]);

        $page = new Page;
        $page->category_id = $request->category_id;
        $page->title = $request->title;
        $page->slug = $request->slug;
        $page->description = $request->description;
        $page->subtitle = $request->subtitle;
        $page->metatitle = $request->metatitle;
        $page->metadescription = $request->metadescription;
        $page->metakeywords = $request->metakeywords;
        $page->order = $page->pageOrder();
        $page->save();

        $page = Page::all()->last();

        if($request->submitButton == 'save'){
            return redirect("/admin/pages/$page->id/edit");
        } elseif ($request->submitButton == 'save-close') {
            return redirect("/admin/pages");
        } elseif ($request->submitButton == 'save-new'){
            return redirect("/admin/pages/create");
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        $categories = Category::all();

        return view('admin.pages.edit', compact('page', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        request()->validate([
            'category_id' => 'required',
            'title' => 'required|min:5',
            'slug' => 'required|min:5',
            'description' => 'required|min:20',
            'subtitle' => 'required|min:min:5',
            'metatitle' => 'required|min:5|max:80',
            'metadescription' => 'required|min:5|max:180'
        ]);

        $page->category_id = $request->category_id;
        $page->title = $request->title;
        $page->slug = $request->slug;
        $page->description = $request->description;
        $page->subtitle = $request->subtitle;
        $page->metatitle = $request->metatitle;
        $page->metadescription = $request->metadescription;
        $page->metakeywords = $request->metakeywords;
        $page->save();
        
        if($request->submitButton == 'save'){
            return redirect("/admin/pages/$page->id/edit");
        } elseif ($request->submitButton == 'save-close') {
            return redirect("/admin/pages");
        } elseif ($request->submitButton == 'save-new'){
            return redirect("/admin/pages/create");
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        $page->delete();

        return redirect('/admin/pages');
    }

    public function order(){

        $orders = $_POST['order'];

     

        foreach ($orders as $neworder => $pageId) {

            $page = Page::find($pageId);
            $page->order = $neworder+1;
            $page->save();

        }
    }
}
