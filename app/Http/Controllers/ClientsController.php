<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients=Client::all();
        return view ('admin.clients.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = request()->validate([
            'name' => 'required',
            'email' => ['required', Rule::unique('clients')]
        ]);

        $attributes['password'] = Hash::make(request('password'));

        Client::create($attributes);

        $id = Client::all()->last()->id;

        if($request->submitButton == 'save'){
            return redirect("/admin/clients/$id/edit");
        } elseif ($request->submitButton == 'save-close') {
            return redirect("/admin/clients");
        } elseif ($request->submitButton == 'save-new'){
            return redirect("/admin/clients/create");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        return view('admin.clients.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $attributes = request()->validate([
            'name' => 'required',
            'email' => ['required']
        ]);

        if(request('password') === $client->password){
            $attributes['password'] = $client->password;
        }
        $attributes['password'] = Hash::make(request('password'));

        $client->update($attributes);

        if($request->submitButton == 'save'){
            return redirect("/admin/clients/$client->id/edit");
        } elseif ($request->submitButton == 'save-close') {
            return redirect("/admin/clients");
        } elseif ($request->submitButton == 'save-new'){
            return redirect("/admin/clients/create");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $client->delete();

        return redirect('/admin/clients');
    }

    public function search (Request $request) {

        $query = request('query');

        $clients = Client::where('name', 'LIKE', '%'. $query .'%')->get();
        

        

        return view('admin.clients.result', compact('clients', 'query'));

    }
}
