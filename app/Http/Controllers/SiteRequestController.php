<?php

namespace App\Http\Controllers;

use App\ContactRequest;
use App\Mail\RequestForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SiteRequestController extends Controller
{
    public function contact(Request $request)
    {
        $request->validate([
                'title' => 'required|min:5',
                'name' => 'required|min:5',
                'phone' => 'required|min:10|regex:/^([0-9\s\-\+\(\)]*)$/',
                'email' => 'required|email',
                'message' => 'required|min:20',
                'terms' => 'required'
            ],
            [
                'title' => trans('validation.title'),
                'phone' => trans('validation.phone'),
                
            ]
        );

        $contactRequest = new ContactRequest();
        $contactRequest->title = $request->title;
        $contactRequest->name = $request->name;
        $contactRequest->phone = $request->phone;
        $contactRequest->email = $request->email;
        $contactRequest->website = $request->website;
        $contactRequest->message = $request->message;
        $contactRequest->save();

        Mail::send(new RequestForm($request));

        // Redirect
        return redirect()->back()->with('message', 'Το μήνυμα στάλθηκε με επιτυχία');

    }
}
