<?php

namespace App\Http\Controllers;

use App\InfoPage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AdminInfoPagesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $infoPages = InfoPage::all()->sortBY('order');
        return view('admin.infopages.index', compact('infoPages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.infopages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        InfoPage::validation();

        $infoPage = new InfoPage();
        $infoPage->title = request('title');
        $infoPage->slug = request('slug');
        $infoPage->metatitle = request('metatitle');
        $infoPage->metadescription = request('metadescription');
        $infoPage->body = request('body');
        $infoPage->alt = request('alt');
        $infoPage->order = $infoPage->pageOrder();


        if($request->hasFile('image')){
            $file = $request->file('image');
            $ext = $file->getClientOriginalExtension();
            $fileNameBase = $request->slug;
            $fileName = $fileNameBase.'.'.$ext;
            $file->storeAs("infopages/", $fileName);
            $infoPage->image = $fileName;
        }

        $infoPage->save();

        $infoPage = InfoPage::all()->last();

        if($request->submitButton == 'save'){
            return redirect("/admin/infopages/$infoPage->id/edit");
        } elseif ($request->submitButton == 'save-close') {
            return redirect("/admin/infopages");
        } elseif ($request->submitButton == 'save-new'){
            return redirect("/admin/infopages/create");
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InfoPage  $infoPage
     * @return \Illuminate\Http\Response
     */
    public function show(InfoPage $infoPage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InfoPage  $infoPage
     * @return \Illuminate\Http\Response
     */
    public function edit(InfoPage $infopage)
    {
        return view('admin.infopages.edit', compact('infopage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InfoPage  $infoPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InfoPage $infopage)
    {
        infoPage::validation();

        if($request->hasFile('image')) {

            $file = $request->file('image');
            $ext = $file->getClientOriginalExtension();
            $fileNameBase = $request->slug;
            $fileName = $fileNameBase.'.'.$ext;
            $file->storeAs("infopages", $fileName);
            $infopage->image = $fileName;

        } elseif ($request->delete === 'checked') {

            Storage::delete("/infopages/$infopage->image");
            $infopage->image = null;            

        } else {
            $infopage->image = $infopage->image;
        }

        $infopage->title = request('title');
        $infopage->slug = request('slug');
        $infopage->metatitle = request('metatitle');
        $infopage->metadescription = request('metadescription');
        $infopage->body = request('body');
        $infopage->alt = request('alt');
        $infopage->order = $infopage->order;
        $infopage->save();

        if($request->submitButton == 'save'){
            return redirect("/admin/infopages/$infopage->id/edit");
        } elseif ($request->submitButton == 'save-close') {
            return redirect("/admin/infopages");
        } elseif ($request->submitButton == 'save-new'){
            return redirect("/admin/infopages/create");
        }

    }

    public function contact()
    {
        # code...
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InfoPage  $infoPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(InfoPage $infopage)
    {
        Storage::delete("/infopages/$infopage->image");

        $infopage->delete();

        return redirect('/admin/infopages');
    }

    public function order(){

        $orders = $_POST['order'];     

        foreach ($orders as $neworder => $pageId) {

            $page = InfoPage::find($pageId);
            $page->order = $neworder+1;
            $page->save();

        }
    }
}
