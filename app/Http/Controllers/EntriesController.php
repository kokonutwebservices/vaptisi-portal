<?php

namespace App\Http\Controllers;

use Storage;
use App\Shop;
use App\Entry;
use App\Image;
use App\Client;
use InterImage;
use App\Category;
use App\District;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class EntriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Entry $entry)
    {
        $entries = $entry->all();
        
        return view('admin.entries.index', compact('entries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Client $client, District $district, Category $category)
    {
        $clients = $client->all();
        $districts = $district->all();
        $categories = $category->where('parent_id', '>', 0)->get();
        return view('admin.entries.create', compact('clients', 'districts', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      

        $request->validate([
            'type' => 'required',
            'client_id' => 'required',
            'category_id' => 'required',
            // 'district_id' => 'required',
            'name' => 'required|min:3',
            'slug' => 'required|min:3',
            // 'logo' => 'required|image|mimes:jpeg,jpg,png|max:512',
            'image' => ['required', ],
            'profile' => 'required|min:100',
            'metatitle' => 'required|max:80',
            'metadescription' => 'required|max:160',
        ]);

        // dd($request->image);
        
        $entry = new Entry();
        $entry->type = $request->type;
        $entry->client_id = $request->client_id;
        $entry->category_id = $request->category_id;
        $entry->name = $request->name;
        $entry->slug = $request->slug;
        $entry->profile = $request->profile;
        $entry->metatitle = $request->metatitle;
        $entry->metadescription = $request->metadescription;
        $entry->metakeywords = $request->metakeywords;
        $entry->website = preg_replace('#^https?://#','',$request->website);
        $entry->facebook = $request->facebook;
        $entry->instagram = $request->instagram;
        $entry->newlife = $request->newlife;
        $entry->active = $request->active;

        $entry->start_at = $entry->startDate($request);
        $entry->end_at = $entry->endDate($request);


        if (count(Entry::all()) > 0 ) {
            $entry->order = Entry::all()->last()->order + 1;
        } else $entry->order = 1; 

        // $category_id = request('category_id');

        //Logo
        if ($request->hasFile('logo')){
            $file = $request->file('logo');
            $ext = $file->getClientOriginalExtension();
            $fileNameBase = request('slug');
            $fileName = $fileNameBase.'-logo.'.$ext;
            $file->storeAs("logos/", $fileName);

            $entry->logo = $fileName;        
        } else {
            $entry->logo = ' ';
        }

        //Profile Image
        if ($request->hasFile('image')){
            $file = $request->file('image');
            $ext = $file->getClientOriginalExtension();
            $fileNameBase = request('slug').'-'.$entry->category_id;
            $fileName = $fileNameBase.'-main.'.$ext;
            $file->storeAs("entries/", $fileName);

            $file = InterImage::make(public_path("storage/entries/$fileName"))->resize(350, null, function($constraint){
                $constraint->aspectRatio();
            })->save(public_path("storage/entries/small/$fileName"));

            $entry->image = $fileName;        
        }

        $entry->save();

        $entry = Entry::all()->last();

        // Ενημέρωση πίνακα district_entry
        $entry->district()->sync($request->district);

        $id = $entry->id;

        // Για κάθε κατάστημα, αποθηκεύεται και μια ξεχωριστή εγγραφή στον πίνακα Shops.
        for ($i=0; $i < 2 ; $i++) { 

            // Αν υπάρχει έστω ένα στοιχείο...
            if($request->address[$i] || $request->tel[$i] || $request->mob[$i] || $request->email[$i] || $request->city[$i]){
                $shop = new Shop;

                $shop->entry_id = $entry->id;
                $shop->address = $request->address[$i];
                $shop->tel = $request->tel[$i];
                $shop->mob = $request->mob[$i];
                $shop->email = $request->email[$i];
                $shop->city = $request->city[$i];
                $shop->save(); 
            }
                       
        }      

        if($request->submitButton == 'save'){
            return redirect("/admin/entries/$id/edit");
        } elseif ($request->submitButton == 'save-close') {
            return redirect("/admin/entries");
        } elseif ($request->submitButton == 'save-new'){
            return redirect("/admin/entries/create");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Entry $entry)
    {
        $clients = Client::all();
        $categories = Category::where('parent_id', '>', 0)->get();
        $districts = District::all();
        $shops = $entry->shops()->get();

        return view('admin.entries.edit', compact(['entry', 'clients', 'categories', 'districts', 'shops']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Entry $entry, Shop $shop)
    {
        
        $attributes = request()->validate([
            'type' => 'required',
            'client_id' => 'required',
            'category_id' => 'required',
            'name' => 'required|min:3',
            'slug' => 'required|min:3',
            // 'logo' => 'image|mimes:jpeg,jpg,png|max:512',
            // 'image' => 'image|max:512',
            'profile' => 'required|min:100',
            'metatitle' => 'required|max:80',
            'metadescription' => 'required|max:160',
        ]);

        
        $attributes['metakeywords'] = request('metakeywords');
        $attributes['website'] = preg_replace('#^https?://#','',$request->input('website'));
        $attributes['facebook'] = request('facebook');
        $attributes['instagram'] = request('instagram');
        $attributes['newlife'] = request('newlife');
        $attributes['active'] = request('active');
        
        $attributes['start_at'] = Carbon::createFromFormat('d/m/Y', $request->start_at)->format('Y/m/d');


        $attributes['end_at'] = Carbon::createFromFormat('d/m/Y', $request->end_at)->format('Y/m/d');

        $attributes['order'] = $entry->order;
    

         //Logo
         if ($request->hasFile('logo')){
            $file = $request->file('logo');
            $ext = $file->getClientOriginalExtension();
            $fileNameBase = request('slug');
            $fileName = $fileNameBase.'-logo.'.$ext;
            $file->storeAs("logos/", $fileName);

            $attributes['logo'] = $fileName;        
        }
        $attributes['logo'] = $entry->logo;

        //Profile Image
        if ($request->hasFile('image')){
            $file = $request->file('image');
            $ext = $file->getClientOriginalExtension();
            $fileNameBase = request('slug').'-'.$attributes['category_id'];
            $fileName = $fileNameBase.'-main.'.$ext;
            $file->storeAs("entries/", $fileName);

            $file = InterImage::make(public_path("storage/entries/$fileName"))->resize(350, null, function($constraint){
                $constraint->aspectRatio();
            })->save(public_path("storage/entries/small/$fileName"));

            $attributes['image'] = $fileName;        
        } else {
            $attributes['image'] = $entry->image;
        }
        
        
        $entry->update($attributes);

        // Ενημέρωση πίνακα district_entry
        $entry->district()->sync($request->district);


        // Loop για κάθε κατάστημα που γίνεται edit
        for ($i=0; $i < 2 ; $i++) { 
            
            // Ενημερώνονται πρώτα τα υπάρχοντα, αν υπάρχουν
            while ($i < count($entry->shops)) {
                $shop = $entry->shops[$i];
                $shop->entry_id = $entry->id;
                $shop->address = $request->address[$i];
                $shop->tel = $request->tel[$i];
                $shop->mob = $request->mob[$i];
                $shop->email = $request->email[$i];
                $shop->city = $request->city[$i];
                $shop->save(); 
                $i++;
            }
        
            if(count($entry->shops) < 2){
                // Αλλιώς προστίθενται μόνο νέα
                $shop = new Shop;
                $shop->entry_id = $entry->id;
                $shop->address = $request->address[$i];
                $shop->tel = $request->tel[$i];
                $shop->mob = $request->mob[$i];
                $shop->email = $request->email[$i];
                $shop->city = $request->city[$i];
                $shop->save(); 
            }
        }
        

        if($request->submitButton == 'save'){
            return redirect("/admin/entries/$entry->id/edit");
        } elseif ($request->submitButton == 'save-close') {
            return redirect("/admin/entries");
        } elseif ($request->submitButton == 'save-new'){
            return redirect("/admin/entries/create");
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entry $entry, Shop $shop)
    {

        foreach ($entry->albums as $album) {
            
            // Διαγραφή φωτογραφιών
            $images = Image::where('album_id', $album->id)->get();

            foreach ($images as $image){

                $image->delete();

            }
            
            // Διαγραφή album
            $album->delete();
        }

        // Διαγραφή του φακέλου με τα album
        Storage::deleteDirectory("uploads/$entry->id");
    
        // Διαγραφή logo
        Storage::delete("logos/$entry->logo");

        // Διαγραφή κεντρικής εικόνας
        Storage::delete("/entries/$entry->image");
        Storage::delete("/entries/small/$entry->image");

        // Διαγραφή καταστημάτων
        $shops = Shop::where('entry_id', $entry->id)->get();

        foreach ($shops as $shop) {
            $shop->delete();
        }

        $entry->delete();

        return redirect('/admin/entries');
    }


    public function deleteShop($shop) {

        $shop = Shop::find($shop);
        
        $shop->delete(); 

    }

    public function search(Request $request) {
        
        $query = request('query');

        $entries = Entry::where('name', 'LIKE', '%'. $query .'%')->get();

        return view('admin.entries.result', compact('entries', 'query'));

    }

    public function messages(Entry $entry)
    {
        return view('admin.entries.messages', compact('entry'));
    }
}
