<?php

namespace App\Http\Controllers;

use App\Album;
use App\Entry;
use App\Image;
use App\Tag;
use Storage;
use Illuminate\Http\Request;

class AlbumsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Entry $entry)
    {
        $albums = $entry->albums->sortBy('order');
        return view('admin.albums.index', compact('entry','albums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Entry $entry)
    {

        return view('admin.albums.create', compact('entry'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Entry $entry)
    {
        request()->validate([
            'name' => 'required'
        ]);

        $album = new Album;
        $album->name = request('name');
        $album->description = request('description');
        $album->entry_id = $entry->id;
        $album->order = $album->albumsOrder();
        $album->save();


        return redirect("/admin/entries/$entry->id/albums");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function show(Album $album)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function edit(Entry $entry, Album $album)
    {
        $images = Image::where('album_id', $album->id)->get();
        $avail = 24-count($images);
        $entryAlbums = $entry->albums()->get();
        $tags = Tag::all();

        return view('admin.albums.edit', compact('entry', 'album', 'images', 'avail', 'entryAlbums', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Entry $entry, Album $album)
    {

        $attributes = request()->validate([
            'name' => 'required|min:5'
        ]);

        $attributes['description'] = request('description');

        $album->update($attributes);

        return redirect()->back();
    
    }

    public function albumCover(Request $request, Entry $entry, Album $album, Image $image){
        
        $album->cover = $request->image;

        $album->save();

        return redirect()->back();

    }

    public function changeAlbum(Request $request, Entry $entry, Album $album, Image $image){
        
        $image->album_id = $request->newAlbum;

        // Μετακίνηση εικόνας
        $filename = $image->image;
        $oldFilePath = "/uploads/$entry->id/$album->id/$filename";
        $newFilePath = "/uploads/$entry->id/$request->newAlbum/$filename";
        Storage::move($oldFilePath, $newFilePath);

        $image->order = Image::imageOrder();

        $image->save();

        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entry $entry, Album $album)
    {
        
        $images = $album->images()->get();

        foreach ($images as $image) {
            
            $image->delete();

        }

        Storage::deleteDirectory("uploads/$entry->id/$album->id/");

        $album->delete();

        return redirect("/admin/entries/$entry->id/albums");        

    }

    public function order (Entry $entry)
    {
        $order  = $_POST['order'];
        
        foreach($order as $neworder=>$item){

            $albumId = explode("-", $item)[1];

            $album = Album::find($albumId);

            $album->order = $neworder+1;

            $album->save();

        }
    }

    public function tags (Request $request, Entry $entry, Album $album, Image $image) {

        // dd($request->tags);

        $image->tags()->sync($request->tags);

        return redirect()->back();

    }
}
