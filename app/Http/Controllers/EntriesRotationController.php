<?php

namespace App\Http\Controllers;

use App\Category;
use App\Entry;

class EntriesRotationController extends Controller
{
    public function entriesRotation()
    {
        $categories = Category::all();
        foreach ($categories as  $category) {
            
            $entries = Entry::where('category_id', $category->id)->get();

            foreach ($entries as  $entry) {
                
                if($entry->order == $entries->max('order')) {
                    $entry->order = 1;
                    $entry->save();
                } else {
                    $entry->order ++;
                    $entry->save();
                }

            }
            
        }
    }
}
