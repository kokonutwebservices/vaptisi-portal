<?php

namespace App\Http\Controllers;

use App\ContactRequest;
use Illuminate\Http\Request;

class AdminRequestsController extends Controller
{
    public function index()
    {
        $contactRequests = ContactRequest::all();

        return view('admin.requests.index', compact('contactRequests'));
    }

    public function show(ContactRequest $contactRequest)
    {
        return view('admin.requests.show', compact('contactRequest'));
    }

    public function destroy(ContactRequest $contactRequest)
    {
        $contactRequest->delete();

        return redirect()->route('admin.request.index');    
    }
}
