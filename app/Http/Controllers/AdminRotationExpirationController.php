<?php

namespace App\Http\Controllers;

use App\Category;
use App\Entry;
use App\Mail\EntryDeactivation;
use App\Mail\EntryExpiration;
use App\Mail\EntryToExpire;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AdminRotationExpirationController extends Controller
{

    public function initialOrder()
    {
        // Αρχική δημιουργία σειρας
        $categories = Category::where('parent_id', '>', '0')->orderBy('order')->get();

        foreach ($categories as $category) {
            $order = 0;
            foreach ($category->entries as $entry) {
                // echo $entry->name . ' ', $order +1 .'<br>' ;
                $entry->order = $order +1;
                $entry->save();
                $order ++;
            }
        }
    }

    public function rotation()
    {
        $categories = Category::where('parent_id', '>', '0')->orderBy('order')->get();

        foreach ($categories as $category) {
            foreach ($category->entries as $entry) {
                if($entry->order == count($category->entries)) {
                    $entry->order = 1;
                    echo $entry->name .'-'. $entry->order . '<br>';
                    $entry->save();
                } else {
                    $entry->order = ($entry->order + 1);
                    echo $entry->name .'-'. $entry->order . '<br>';
                    $entry->save();
                }
            }
        }        
    }

    public function expire()
    {

        $entriesExpired = Entry::where('end_at', '<', Carbon::now())->orderBy('end_at', 'asc')->get();

        foreach ($entriesExpired as $entry) {
            // $entry->active = 1;
            // $entry->save();

            if(Carbon::parse($entry->end_at)->addMonth() < Carbon::now() ) {

                // Απενεργοποίηση
                $entry->active = 0;
                $entry->save();
                Mail::send(new EntryDeactivation($entry));

            } else {

                // Ειδοποίηση λήξης
                Mail::send(new EntryExpiration($entry));

            }

        }

        $entriesToExpire = ENtry::where([['end_at', '>', Carbon::now()], ['end_at', '<', Carbon::now()->addMonth()]])->get();

        foreach ($entriesToExpire as $entry) {

            Mail::send(new EntryToExpire($entry));

        }
        

    }
}
