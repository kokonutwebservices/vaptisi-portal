<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $guarded = [];

    public function entry(){
        return $this->belongsTo(Entry::class);
    }

    public function newVideoOrder(){

        return $this->max('order') + 1;
    }
}
