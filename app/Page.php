<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $guarded = [];

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function entries()
    {
        return $this->hasMany(Entry::class);
    }

    public function pageOrder(){
        return $this->max('order')+1;
    }
}
