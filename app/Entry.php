<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Entry extends Model
{
    protected $guarded = [];

    public function client () {
        return $this->belongsTo(Client::class);
    }

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function district() {
        return $this->belongsToMany(District::class);
    }

    public function shops () {
        return $this->hasMany(Shop::class, 'entry_id');
    }

    public function albums(){
        return $this->hasMany(Album::class, 'entry_id');
    }

    public function videos(){
        return $this->hasMany(Video::class, 'entry_id');
    }

    public function images() {
        return $this->hasMany(Image::class, 'entry_id');
    }

    public function messages()
    {
        return $this->hasMany(ClientMessage::class);
    }

    public function showEntryType() {
        if ($this->type === 'basic') {
            echo 'Basic';
        } elseif ($this->type === 'premium') {
            echo 'Premium';
        }
    }
    
   public function startDate($request)
   {
       if($request->start_at) {
           return $this->attributes['start_at'] = $request->start_at;
       }
       return $this->attributes['start_at'] = Carbon::now();
   }

    public function endDate($request)
    {
        // $start = Carbon::createFromFormat('d/m/Y', $request->start_at);

        return Carbon::parse($request->start_at)->addYear();
    }

    public static function cleanLink($link)
    {
        if(!preg_match('#^https?://#', $link)) {
            return $link = 'https://'.$link;
        }

        return $link;
    }

}