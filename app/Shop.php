<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $guarded = [];

    public function basicentry () {
        return $this->belongsTo(Entry::class, 'entry_id');
    }
}
