<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];


    public function entries(){
        return $this->hasMany(Entry::class);
    }

    public function categoryOrder(){
        return $this->max('order')+1;
    }

    public function categoryEntries($district) {

        return count($district->entries->where('category_id', $this->id));
    }

    public function page () {
        return $this->hasOne(Page::class);
    }

    // Τεχνική από εδώ: https://laracasts.com/series/laravel-6-from-scratch/episodes/28
    public function adminPath() {
        return '/admin/categories/'.$this->id;
    }
}
