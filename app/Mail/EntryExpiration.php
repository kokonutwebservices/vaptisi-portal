<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EntryExpiration extends Mailable
{
    use Queueable, SerializesModels;


    public $entry;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($entry)
    {
        $this->entry = $entry;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        return $this->markdown('site.emails.entryexpiration')
        ->to('info@vaptisiportal.gr')
        ->from('info@vaptisiportal.gr')
        ->subject('Λήγει η καταχώρηση '. $this->entry->name);
    }
}
