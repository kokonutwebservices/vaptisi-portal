<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EntryContactForm extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $message;
    public $entry;
    public $category;

    public function __construct($message, $entry, $category)
    {
        $this->message = $message;
        $this->entry = $entry;
        $this->category = $category;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // dd($this->message);
        return $this->markdown('site.emails.entrycontactform')
            ->to($this->entry->client->email)
            ->replyTo($this->message->email)
            ->subject('Vaptisi Portal - Έχετε μήνυμα!')
            ->from('info@vaptisiportal.gr');
    }
}
