<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EntryWebsiteNotification extends Mailable
{
    use Queueable, SerializesModels;


    public $shop;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($shop)
    {
        $this->shop = $shop;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('site.emails.websitenotification')
        ->to('zervas@gamosportal.gr')
        ->subject("Ο πελάτης ".$this->shop->basicentry->name. " είχε website click")
        ->from('info@vaptisiportal.gr', 'Vaptisi Portal');
    }
}
