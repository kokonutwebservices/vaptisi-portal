<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EntryContactFormCopy extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $message;
    public $category;
    public $entry;
    

    public function __construct($message, $category, $entry)
    {
        $this->message = $message;
        $this->category = $category;
        $this->entry = $entry;        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('site.emails.entrycontactformcopy')
        ->to('zervas@gamosportal.gr')
        ->from('info@vaptisiportal.gr')
        ->subject('Μήνυμα για την καταχώρηση '. $this->entry->name);
    }
}
