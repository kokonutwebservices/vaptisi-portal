<?php

namespace App\Mail;

use App\Shop;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EntryPhoneNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $shop;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Shop $shop)
    {
        $this->shop = $shop;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('site.emails.entryphonenotification')
        ->to('zervas@gamosportal.gr')
        ->subject("Ο πελάτης ".$this->shop->basicentry->name. " είχε τηλεφωνική κλήση")
        ->from('info@vaptisiportal.gr', 'Vaptisi Portal');
    }
}
