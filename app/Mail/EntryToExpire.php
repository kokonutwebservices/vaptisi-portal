<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EntryToExpire extends Mailable
{
    use Queueable, SerializesModels;

    public $entry;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($entry)
    {
        $this->entry = $entry;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('site.emails.entrytoexpire')
        ->to('info@vaptisiportal.gr')
        ->from('info@vaptisiportal.gr', 'Vaptisi Portal')
        ->subject("Η καταχώρηση " .$this->entry->name. " λήγει");
    }
}
