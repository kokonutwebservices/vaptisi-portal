<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientMessage extends Model
{

    protected $guarded = [];

    public function entry()
    {
        return $this->belongsTo(Entry::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
