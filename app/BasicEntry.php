<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class BasicEntry extends Model
{
    protected $guarded = [];

    public function client () {
        return $this->belongsTo(Client::class);
    }

    public function category () {
        return $this->belongsTo(Category::class);
    }

    public function shops () {
        return $this->hasMany(Shop::class, 'entry_id');
    }
}
