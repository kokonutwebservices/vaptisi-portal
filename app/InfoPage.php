<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoPage extends Model
{
    protected $guarded = [];


    public function pageOrder(){
        return $this->max('order')+1;
    }


    public static function validation() {

        return request()->validate([
            'title' => 'required',
            'slug' => 'required',
            'metatitle' => 'required'
        ]);
    }
}
