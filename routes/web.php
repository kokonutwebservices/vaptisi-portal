<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth::loginUsingId(1);
// Auth::logout();

use App\ClientMessage;
use App\Http\Controllers\SitemapController;

Auth::routes();


Route::group(['prefix' => '/admin', 'middleware' => 'auth'],function(){
// Route::group(['prefix' => '/admin'],function(){

    Route::get('/messages/fix', function(){
        $messages = ClientMessage::with('entry')->get();

        foreach ($messages as $message) {
            $message->update([
                'entry_name' => $message->entry->name
            ]);
        }
    });
    
    Route::get('', 'AdminController@index');

    Route::resource('/users', 'AdminUsersController');
    Route::resource('/abilities', 'AdminUserAbilitiesController');
    Route::resource('/roles', 'AdminUserRolesController');

    Route::resource('/clients', 'ClientsController');
    Route::post('/clients/search', 'ClientsController@search');
    Route::resource('/categories', 'AdminCategoriesController');
    Route::post('/categories/order', 'AdminCategoriesController@order');
    Route::post('/categories/suborder', 'AdminCategoriesController@suborder');
    Route::resource('/districts', 'DistrictsController');
    Route::get('/entries/thumbs', 'ImagesController@thumbs'); //script για δημιουργία Thumbnails
    
    Route::resource('/entries', 'EntriesController');
    Route::delete('/shop/{shop}/delete', 'EntriesController@deleteShop');
    Route::post('/entries/search', 'EntriesController@search');
    Route::get('/entries/{entry}/messages', 'EntriesController@messages');
    Route::resource('/entries/{entry}/albums', 'AlbumsController');
    Route::post('/entries/{entry}/albums/order', 'AlbumsController@order');
    Route::post('/entries/{entry}/albums/{album}/images/cover', 'AlbumsController@albumCover');
    Route::resource('/entries/{entry}/albums/{album}/images', 'ImagesController');

    Route::post('/entries/{entry}/albums/{album}/images/order', 'ImagesController@order');

    Route::post('/entries/{entry}/albums/{album}/images/{image}/description', 'ImagesController@imageDescriptionStore');
    Route::post('entries/{entry}/albums/{album}/{image}/changealbum', 'AlbumsController@changeAlbum');
    Route::post('entries/{entry}/albums/{album}/{image}/tags', 'AlbumsController@tags');
    Route::get('/entries/{entry}/albums/{album}/{image}/delete', 'ImagesController@destroy');
    Route::resource('/entries/{entry}/video', 'VideoController');
    Route::get('/entries/{entry}/video/{video}/delete', 'VideoController@destroy');
    Route::post('/entries/{entry}/video/order', 'VideoController@order');
    Route::resource('/pages', 'PagesController');
    Route::post('/pages/order', 'PagesController@order');
    Route::resource('/tags', 'TagsController');

    Route::resource('/infopages', 'AdminInfoPagesController');
    Route::post('/infopages/order', 'AdminInfoPagesController@order');

    Route::get('/requests', 'AdminRequestsController@index')->name('admin.request.index');
    Route::get('requests/{contactRequest}', 'AdminRequestsController@show')->name('admin.request.show');
    Route::delete('/requests/{contactRequest}', 'AdminRequestsController@destroy')->name('admin.request.delete');

    Route::resource('/messages', 'AdminClientMessagesController')->name('*','messages');

});

Route::get('/rotation', 'AdminRotationExpirationController@rotation');
Route::get('/initial', 'AdminRotationExpirationController@initialOrder');
Route::get('expiration', 'AdminRotationExpirationController@expire');
Route::get('/sitemap.xml', 'SitemapController@index');

Route::get('/', 'SiteHomePageController@index');
Route::get('/{categorypage}', 'SiteCategoryController@index');
Route::post('/contact', 'SiteRequestController@contact');
Route::get('/{categorypage}/p-{district}', 'SiteCategoryController@district');
Route::get('/{categorypage}/{entryslug}', 'SiteEntryController@show');
Route::post('/{category}/{entry}/contactform', 'SiteEntryController@contactForm');
Route::get('rotation', 'EntriesRotationController@entriesRotation');